#!/usr/bin/env python3

import sys

with open(sys.argv[1], 'rb') as f:
    data = f.read()

for page_i in range(0x8):
    acc = 0
    for i in range(0x10000):
        acc += data[0x10000 * page_i + i]
    print(page_i, hex(acc & 0xffff))
