#!/usr/bin/env python3

from PIL import Image
from pathlib import Path
import struct
import numpy as np
import sys


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


p_dat = 0x00225560
p_hdr_meta = 0x06098CA8
with open(sys.argv[1], "rb") as f:
    hdr = f.read()
with open(sys.argv[2], "rb") as f:
    dat = f.read()
with open(sys.argv[3], "r+b") as f:
    f.seek(0x00225560)
    f.write(dat)

    for i in range(len(hdr) // 0x10):
        idx, size_idx, h, w, pal_idx, is_present, size, acc = struct.unpack(
            "<HHBBBBLL", hdr[i * 0x10 : (i + 1) * 0x10]
        )
        w = w // 8
        print(
            "{:04X} {:04X} {:02X} {:02X} {:02X} {:02X} {:08X} {:08X}".format(
                idx, size_idx, h, w, pal_idx, is_present, size, acc
            )
        )

        if size == 0:
            continue  # Missing entries :(

        # 0000 0000 00 00 00 00 00000000 00000000
        # 0000 0001 00 00 00 00 00000000 00000000
        # 0000 0002 00 00 00 00 00000000 00000000
        # 0001 0000 05 01 00 01 00000013 00000000
        # 0001 0001 08 02 00 01 00000046 00000013
        # 0001 0002 11 04 00 01 0000011F 00000059
        entry = struct.pack(
            ">BBHBBBBLL", w, h, 0, pal_idx, 0, 0, 0x20, 0x225560 + acc, 0x060516ba
        )
        # print(entry)
        f.seek(0x06098ca8 + (i * 0x10))
        f.write(entry)
