.set noreorder

.global __start
.text
__start:
lw $s0,0xc($v0)
nop
lb $s0,0x4($s0)
ori $v1,$0,0x4
bne $v1,$s0,cave_end

ori $v1,$0,0x7
lw $s0,0xc($v0)
nop
sb $v1,0x4($s0)
ori $v1,$0,0x0
sb $v1,0x6($s0)

cave_end:
lw $v0,0x8($v0)
j 0x80031604
nop
nop
