# TODO write a description for this script
# @author
# @category _NEW_
# @keybinding
# @menupath
# @toolbar

"""
### original from zcutlib ###
https://gist.github.com/zcutlip/0b3662574ecb506158ad4d3a288736b8

### edited by tobitlill ###
Ghidra script to colorize all basic blocks identified by the input file.
Prompts for a file to use as input, marks the corresponding basic blocks
in the listing and makes selection in the decompile view. Additionally, some
info about the visited basic blocks is printed to the console.

Ghidra does not support colored markers in the decompile ATM so that is an attempt
to view the visited parts anyway.

Input should be a line-separated list of offsets from start of the program:

$ cat bb_list.txt
000001de02
000001de19
000001de1f
    ...
"""

from java.awt import Color
from ghidra.program.model.block import BasicBlockModel
from ghidra.util.task import TaskMonitor, ConsoleTaskMonitor
from ghidra.app.plugin.core.colorizer import ColorizingService
from docking.options.editor import GhidraColorChooser
from ghidra.program.model.address import AddressSet
from ghidra.program.util import ProgramSelection


def to_addresses(lines):
    addresses = []
    for line in sorted(lines):
        offset = str(line.strip())
        address = (
            currentProgram.getAddressFactory()
            .getDefaultAddressSpace()
            .getAddress(offset)
        )
        addresses.append(address)

    return addresses


bbm = BasicBlockModel(currentProgram)
monitor = ConsoleTaskMonitor()
lastBlock = None

# remove previously defined highlights
s = AddressSet()
s.addRange(
    currentProgram.getAddressFactory().getDefaultAddressSpace().getAddress(0x0),
    currentProgram.getAddressFactory().getDefaultAddressSpace().getAddress(0xFFFFF),
)
clearBackgroundColor(s)

# select input files
bb_f1 = askFile("Select Basic Block Address List 1", "Ok")
bb_f1 = str(bb_f1)
if not bb_f1:
    raise RuntimeError("Address List 1 not provided.")
with open(bb_f1, "r") as f:
    addresses1 = to_addresses(f.readlines())

bb_f2 = askFile("Select Basic Block Address List 2", "Ok")
bb_f2 = str(bb_f2)
if bb_f2:
    with open(bb_f2, "r") as f:
        addresses2 = list(set(to_addresses(f.readlines())).difference(addresses1))
else:
    addresses2 = []

colors = [Color(0xC0, 0xC0, 0xFF), Color(0xFF, 0xC0, 0xC0)]
for record in [(addresses1, colors[0], False), (addresses2, colors[1], True)]:
    addresses, color, lastAddresses = record
    for address in addresses:
        # auto analysis may define incorrect data addresses where
        # code for found addresses should be, so we need to clear those
        # to get a valid disassembly
        clearListing(address)

        disassemble(address)

        # block = bbm.getFirstCodeBlockContaining(address, monitor)
        block = ProgramSelection(currentProgram.getAddressFactory(), address, address)
        if block:
            min, max, len = (
                block.minAddress,
                block.maxAddress,
                block.maxAddress.subtract(block.minAddress),
            )
            # print("Coloring Block: ({} - {}, {} Bytes) | {}".format(min, max, len, block.name))
            setBackgroundColor(block, color)

            # get the address of one colored block
            if lastBlock is None:
                lastBlock = block
        else:
            print("No block for {}".format(address))

    if lastBlock and lastAddresses:
        # make selections in the decompile view because no markers are available there
        service = state.getTool().getService(ColorizingService)
        ranges = service.getBackgroundColorAddresses(
            service.getBackgroundColor(lastBlock.minAddress)
        )
        setCurrentSelection(ranges)
