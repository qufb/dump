#include <unicorn/unicorn.h>
#include <udbserver.h>
#include <stdio.h>
#include <stdlib.h>

#define BIOS_SIZE 0x20000
#define TROJAN_SIZE 0xc0000
#define BEENA_4M_MASK_SIZE 0x400000
#define BEENA_8M_MASK_SIZE 0x800000

unsigned char bios[BIOS_SIZE];
unsigned char rom[BEENA_8M_MASK_SIZE];
unsigned char trojan[TROJAN_SIZE];

static bool hook_mem_fetch_unmapped(uc_engine *uc, uint64_t address) {
    if (address == 0x218 || address == 0x21c) {
        printf("> HOOK @ 0x%" PRIx64 "\n", address);

        uint32_t r0, r1, lr;
        uc_reg_read(uc, UC_ARM_REG_R0, &r0);
        uc_reg_read(uc, UC_ARM_REG_R1, &r1);
        r0 = r0 / r1;
        uc_reg_write(uc, UC_ARM_REG_R0, &r0);

        // bx lr
        uc_reg_read(uc, UC_ARM_REG_LR, &lr);
        uc_reg_write(uc, UC_ARM_REG_PC, &lr);

        return true;
    } else if (address == 0x220 || address == 0x224) {
        printf("> HOOK @ 0x%" PRIx64 "\n", address);

        uint32_t r0, r1, lr;
        uc_reg_read(uc, UC_ARM_REG_R0, &r0);
        uc_reg_read(uc, UC_ARM_REG_R1, &r1);
        r0 = r0 % r1;
        uc_reg_write(uc, UC_ARM_REG_R0, &r0);

        // bx lr
        uc_reg_read(uc, UC_ARM_REG_LR, &lr);
        uc_reg_write(uc, UC_ARM_REG_PC, &lr);

        return true;
    } else if ((address >= 0x0 && address <= 0x2ff) || address == 0xc00ce8b4) {
        printf("> HOOK (NOP'd) @ 0x%" PRIx64 "\n", address);

        uint32_t lr;

        // bx lr
        uc_reg_read(uc, UC_ARM_REG_LR, &lr);
        uc_reg_write(uc, UC_ARM_REG_PC, &lr);

        return true;
    } else if (address == 0x800294bc) {
        // HACK: read_io_page() (0x80029498) is registered in callback chain but never called by us, so we replicate the assignments under parse_page() (0x800294bc).
        const unsigned char input_page_u4_1[4] =  {0xff, 0x00, 0xff, 0x00};
        uc_mem_write(uc, 0x5002002CL, input_page_u4_1, 0x4);
        uc_mem_write(uc, 0xc00d5460L, input_page_u4_1, 0x4);
        const unsigned char input_page_u4_2[4] =  {0xff, 0xff, 0x00, 0xff};
        uc_mem_write(uc, 0x50020030L, input_page_u4_2, 0x4);
        uc_mem_write(uc, 0xc00d5464L, input_page_u4_2, 0x4);
    } else {
        return false;
    }
}

static bool hook_mem_invalid(uc_engine *uc, uc_mem_type type, uint64_t address, int size, int64_t value, void *user_data) {
    switch (type) {
    case UC_MEM_READ_UNMAPPED:
        printf("! UC_MEM_READ_UNMAPPED @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_WRITE_UNMAPPED:
        printf("! UC_MEM_WRITE_UNMAPPED @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_FETCH_UNMAPPED:
        printf("! UC_MEM_FETCH_UNMAPPED @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_READ_PROT:
        printf("! UC_MEM_READ_PROT @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_WRITE_PROT:
        printf("! UC_MEM_WRITE_PROT @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_FETCH_PROT:
        printf("! UC_MEM_FETCH_PROT @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    default:
        printf("! UC_MEM @ 0x%" PRIx64 "\n", address);
        return false;
    }
}

static void hook_code(uc_engine *uc, uint64_t address, uint32_t size, void *user_data) {
    printf("> trace @ 0x%" PRIx64 "\n", address, size);
    hook_mem_fetch_unmapped(uc, address);
}

int main(int argc, char *argv[]) {
    if (argc < 4) {
        fprintf(stderr, "Usage: %s bios.bin rom.bin trojan.bin\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    FILE *fd_bios = fopen(argv[1], "rb");
    fseek(fd_bios, 0, SEEK_END);
    unsigned long bios_size = ftell(fd_bios);
    fseek(fd_bios, 0, SEEK_SET);
    fread(bios, bios_size, 1, fd_bios);
    fclose(fd_bios);

    FILE *fd_rom = fopen(argv[2], "rb");
    fseek(fd_rom, 0, SEEK_END);
    unsigned long rom_size = ftell(fd_rom);
    fseek(fd_rom, 0, SEEK_SET);
    fread(rom, rom_size, 1, fd_rom);
    fclose(fd_rom);

    FILE *fd_trojan = fopen(argv[3], "rb");
    fseek(fd_trojan, 0, SEEK_END);
    unsigned long trojan_size = ftell(fd_trojan);
    fseek(fd_trojan, 0, SEEK_SET);
    fread(trojan, trojan_size, 1, fd_trojan);
    fclose(fd_trojan);

    unsigned long mask_size = (rom_size < BEENA_8M_MASK_SIZE)
        ? BEENA_4M_MASK_SIZE
        : BEENA_8M_MASK_SIZE;
    fprintf(stderr, "Mapping mask size = 0x%08x\n", mask_size);

    unsigned long bios_base = 0;
    unsigned long rom_base = 0x80000000L;
    unsigned long trojan_base = 0xc0000000L;

    unsigned long start = trojan_base + 0x100;
    unsigned long until = trojan_base + 0x158;
    unsigned long bx_r0 = until + 2;

    uc_engine *uc;
    uc_hook trace1, trace2, trace3;
    uc_open(UC_ARCH_ARM, UC_MODE_THUMB + UC_MODE_BIG_ENDIAN, &uc);

    uc_mem_map(uc, bios_base, BIOS_SIZE, UC_PROT_ALL);
    uc_mem_map(uc, 0x20000000L, 0x4000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x40000000L, 0x40000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x50000000L, 0x40000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x60000000L, 0x40000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x70000000L, 0x40000L, UC_PROT_ALL);
    uc_mem_map(uc, rom_base, mask_size, UC_PROT_READ | UC_PROT_EXEC);
    uc_mem_map(uc, 0xA0FFC000L, 0x4000L, UC_PROT_ALL);
    uc_mem_map(uc, 0xC0000000L, 0x200000L, UC_PROT_ALL);

    // Test mode button mask
    const unsigned char input_buttons[4] =  {0x00, 0x00, 0x01, 0xff};
    uc_mem_write(uc, 0x50020034L, input_buttons, 0x4);

    // HACK: This passes a loop in FUN_8002d008()
    const unsigned char input_500300BC[4] =  {0x00, 0x00, 0x00, 0x01};
    uc_mem_write(uc, 0x500300BCL, input_500300BC, 0x4);

    uc_mem_write(uc, bios_base, bios, BIOS_SIZE);
    uc_mem_write(uc, rom_base, rom, mask_size);
    uc_mem_write(uc, trojan_base, trojan, TROJAN_SIZE);
    const unsigned char loop[2] =  {0xe7, 0xfe};
    uc_mem_write(uc, bx_r0, loop, 0x2);

    unsigned long sp = 0xC00FFF80L;
    uc_reg_write(uc, UC_ARM_REG_SP, &sp);

    // https://github.com/unicorn-engine/unicorn/blob/241a391cecacda2b1463b873e6de6a7b9a35ba4c/include/unicorn/unicorn.h#L330
    uc_hook_add(uc, &trace1, UC_HOOK_CODE, hook_code, NULL, 1, 0);
    //uc_hook_add(uc, &trace2, UC_HOOK_MEM_READ, hook_mem_read, NULL, 1, 0);
    uc_hook_add(uc, &trace3,
        UC_HOOK_MEM_READ_PROT |
        UC_HOOK_MEM_WRITE_PROT |
        UC_HOOK_MEM_FETCH_PROT |
        UC_HOOK_MEM_READ_UNMAPPED |
        UC_HOOK_MEM_WRITE_UNMAPPED |
        UC_HOOK_MEM_FETCH_UNMAPPED |
        UC_HOOK_MEM_UNMAPPED,
        hook_mem_invalid, NULL, 1, 0);

    udbserver(uc, 12345, start);

    // https://github.com/unicorn-engine/unicorn/blob/master/samples/sample_arm.c
    uc_emu_start(uc, start | 1, until, 0, 0);

    return 0;
}
