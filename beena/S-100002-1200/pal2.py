#!/usr/bin/env python3

from PIL import Image
import math
import numpy as np
import sys


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


TILE_LEN = 8

input_name = sys.argv[1]
w = int(sys.argv[2], 0)

with open(input_name, "rb") as f:
    input_bytes = f.read()

colors_offset = 0x748f14 # test font
colors_offset = 0x740222 # grayscale
colors_offset = 0x745730
colors_offset = 0x1665be
colors_offset = 0x75288e
colors_offset = 0x64122 # intro with song?
colors_offset = 0x4a538 # text box font
colors_offset = 0x4cdf8 # warn
colors_offset = 0x4d32c # beena logo
colors_offset = 0x53898 # segatoys logo

#kazoku
#colors_offset = 0x74a1c
#colors_offset = 0x74dac
num_entries = int.from_bytes(input_bytes[colors_offset:colors_offset+2], byteorder="big")
colors = input_bytes[colors_offset + 2 : colors_offset + 2 + 2 * num_entries]
rgb_colors = []
rgb_colors.append([255, 255, 255])
for i in range(0, len(colors), 2):
    color = colors[i : i + 2]
    color = int.from_bytes(color, byteorder="big")
    b = (color >> 10) & 0b11111
    g = (color >> 5) & 0b11111
    r = color & 0b11111
    rgb = [int(x * (255 / 31)) for x in [r, g, b]]
    rgb_colors.append(rgb)
for rgb_color in rgb_colors:
    for x in rgb_color:
        print(hex(x)[2:].zfill(2), end='')
print()
#print(rgb_colors)

rgb_pixels = []
for i in rgb_colors:
    for j in range(8 * 8):
        rgb_pixels.append(i)
rgb_tiles = list(chunks(rgb_pixels, 8 * 8))
#print(rgb_tiles)

idata = np.zeros(
    (((len(rgb_tiles) // w) + 1) * TILE_LEN, w * TILE_LEN, 3), dtype=np.uint8
)
tdata = rgb_tiles
for ti in range(len(rgb_tiles)):
    wi = ti % w
    hi = ti // w
    for i in range(0, TILE_LEN):
        for j in range(0, TILE_LEN):
            v = tdata[ti][i + (j * TILE_LEN)]
            idata[(hi * TILE_LEN) + j, (wi * TILE_LEN) + i] = tuple(v)
img = Image.fromarray(idata, "RGB")
img.show()
img.save(f"out2.pal.png")
