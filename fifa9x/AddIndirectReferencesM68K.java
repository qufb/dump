// Resolves call addresses for indirect references in instructions with format `jsr (0x1234,A5)`.
// 
// Preconditions:
// - Jump table instructions manually dissassembled;
// - Blocks with jump calls contained in a function.
// 
// TODO:
// - Handle SUB and FUN in table
// - Handle other instructions
// 
// FIFA96:
// 0006d308 3b 7c ff        move.w     #-0x1,(offset DAT_fffff2c2,A5)
//          ff f2 c2
// 
//@author 
//@category _NEW_
//@keybinding 
//@menupath 
//@toolbar

import java.util.Arrays;

import ghidra.app.script.GhidraScript;
import ghidra.program.model.address.Address;
import ghidra.program.model.address.AddressIterator;
import ghidra.program.model.lang.Register;
import ghidra.program.model.listing.CodeUnit;
import ghidra.program.model.listing.Function;
import ghidra.program.model.listing.Instruction;
import ghidra.program.model.listing.Listing;
import ghidra.program.model.listing.Program;
import ghidra.program.model.symbol.RefType;
import ghidra.program.model.symbol.Reference;
import ghidra.program.model.symbol.SourceType;
import ghidra.util.NumericUtilities;


public class AddIndirectReferencesM68K extends GhidraScript {
	
    @Override
    public void run() throws Exception {
    	final Program program = currentProgram;
    	final Listing listing = program.getListing();

		//println("currentAddress: " + toHex(currentAddress.getUnsignedOffset()));
		//Function func = currentProgram.getFunctionManager().getFunctionContaining(currentAddress);
    	
		final String inputChoice = askChoice("Input Offset", "Choose offset for jump table add expression",
				Arrays.asList(new String[] { "CoachK: 69c", "FIFA95: 69c", "FIFA96: 772", "NBA96: 772" }),
				"FIFA95: 0x69c");
		final long inputOffset = Integer.parseUnsignedInt(inputChoice.split(" ")[1], 16);
    	println(String.format("inputOffset=%s", toHex(inputOffset)));
    	
		Function func = getFirstFunction();
		Function nextFunc;
		while (func != null) {
	    	println("func: " + func);
			AddressIterator addrIter = func.getBody().getAddresses(true);
			while(addrIter.hasNext() && !monitor.isCancelled()) {
				processAddr(listing, addrIter.next(), inputOffset);
			}
			
			nextFunc = getFunctionAfter(func);
			if (nextFunc == null || nextFunc.getEntryPoint().getUnsignedOffset() == func.getEntryPoint().getUnsignedOffset()) {
				break;
			}
			func = nextFunc;
		}
		
    	println("Done!");
    }

    private void processAddr(Listing listing, Address loc, long inputOffset) throws Exception {
    	Instruction ins = listing.getInstructionAt(loc);
    	if (ins == null) {
    		println(String.format("Skipping: Invalid ins, loc=%s.", toHex(loc.getUnsignedOffset())));
    		return;
    	}
    	if (!ins.getMnemonicString().equalsIgnoreCase("jsr")) {
    		println("Skipping: not jsr.");
    		return;
    	}
    	/*
    	if (ins.getInputObjects().length != 2) {
    		println("Skipping: not enough input objects.");
    		return;
    	}
    	*/
    	boolean hasA5 = false;
    	boolean hasSP = false;
    	Long vSP = null;
    	// FIXME: Iterate operands
    	int countRegisters = 0;
    	/*
    	for (int i = 0; i < ins.getNumOperands(); i++) {
			String v =  ins.getDefaultOperandRepresentation(i);
			println(v);
    	}
    	*/
		try {
			for (int i = 0; i < ins.getInputObjects().length; i++) {
				Object in = ins.getInputObjects()[i];
				if (in instanceof Register) {
					String name = ((Register) in).getName();
					if (name.equalsIgnoreCase("SP")) {
						vSP = NumericUtilities.parseHexLong(ins.getDefaultOperandRepresentation(0));
					}
					hasA5 |= name.equalsIgnoreCase("A5");
					hasSP |= name.equalsIgnoreCase("SP");

					countRegisters++;
				}
			}
		} catch (Exception e) {
			println(e.getMessage());
			return;
		}
    	if (countRegisters != 2) {
    		println(String.format("Skipping: mismatched number of registers (expected 2, got %d).", countRegisters));
    		return;
    	}
    	if (!hasA5 || !hasSP) {
    		println("Skipping: mismatched expected registers.");
    		return;
    	}
    	println(Long.toHexString(vSP));
    	
        /*
		for (PcodeOp op : ins.getPcode()) {
			println("r op: " + op);
			println("r op ---: " + op.getMnemonic());
			println("r op ---: " + op.getOutput());
			
		}
    	*/
    	
    	Long srcAddress = loc.getUnsignedOffset();
    	Long vJumpTable = vSP + inputOffset;
    	println(String.format("vJumpTable = %s", toHex(vJumpTable)));
    	addJumpTableRef(srcAddress, vJumpTable);
    	
    	/*
    	DataIterator dataIter = listing.getDefinedData(loc, true);
    	Data data;
		if (dataIter.hasNext() && !monitor.isCancelled()) {
			data = dataIter.next();
			Address currAddr = data.getMinAddress();
			monitor.setMessage(currAddr.toString());
			
			println(data.toString());
		}
		*/
	}

	private void addJumpTableRef(Long srcAddress, Long jAddress) throws Exception {
    	Program program = currentProgram;
    	Listing listing = program.getListing();
		
		Address jLoc = currentAddress.getAddress(toHex(jAddress));
    	Instruction jIns = listing.getInstructionAt(jLoc);

    	if (jIns == null || jIns.getMnemonicString() == null) {
    		println("Skipping: invalid ins.");
    		return;
    	}
    	if (!jIns.getMnemonicString().equalsIgnoreCase("jmp")) {
    		println("Skipping: not jmp.");
    		return;
    	}
    	if (jIns.getNumOperands() != 1) {
    		println("Skipping: mismatched num operands.");
    		return;
    	}
    	String v =  jIns.getDefaultOperandRepresentation(0);
    	println(v);

    	String vjAddress = String.format("0x%x", jAddress);
		Address srcLoc = currentAddress.getAddress(toHex(srcAddress));
    	Instruction srcIns = listing.getInstructionAt(srcLoc);
    	boolean hasV = false;
    	boolean hasJ = false;
    	for (Reference ref : srcIns.getOperandReferences(0)) {
    		hasV |= ref.getToAddress().equals(currentAddress.getAddress(v));
    		hasJ |= ref.getToAddress().equals(currentAddress.getAddress(vjAddress));
    	}
    	if (!hasV) {
        	srcIns.addOperandReference(0, currentAddress.getAddress(v), RefType.UNCONDITIONAL_CALL, SourceType.USER_DEFINED);
    	}
    	if (!hasJ) {
    		final Function jFunc = getFunctionContaining(currentAddress.getAddress(v));
    		final String funcName = (jFunc == null) ? toHex(jAddress) : jFunc.getName();
    		srcIns.setComment(CodeUnit.PRE_COMMENT, String.format("func=%s, jTblAddr=%s", funcName, vjAddress));
    	}
    }

    private void describe(Long vAddress) throws Exception {
    	Program program = currentProgram;
    	Listing listing = program.getListing();
    	
		Address loc = currentAddress.getAddress(toHex(vAddress));
    	Instruction ins = listing.getInstructionAt(loc);
    	println(ins.getMnemonicString());
    	for (int i = 0; i < ins.getNumOperands(); i++) {
			String v =  ins.getDefaultOperandRepresentation(i);
			println(v);
    	}
    }

	private String toHex(long v) {
		return String.format("0x%x", v);
	}
}
