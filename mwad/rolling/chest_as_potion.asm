.set noreorder

.global __start
.text
__start:
lb $s0,0x4($s3)
ori $v1,$0,0x4
bne $v1,$s0,cave_end

ori $v1,$0,0x7
sb $v1,0x4($s3)
ori $v1,$0,0x0
sb $v1,0x6($s3)

cave_end:
lhu $v0,0x4($s3)
j 0x80034848
nop
nop
