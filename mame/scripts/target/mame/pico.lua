-- license:BSD-3-Clause
-- copyright-holders:MAMEdev Team

---------------------------------------------------------------------------
--
--   pico.lua
--
--   Small driver-specific example makefile
--   Use make SUBTARGET=pico to build
--
---------------------------------------------------------------------------


--------------------------------------------------
-- Specify all the CPU cores necessary for the
-- drivers referenced in pico.lst.
--------------------------------------------------

CPUS["M680X0"] = true
CPUS["Z80"] = true


--------------------------------------------------
-- Specify all the sound cores necessary for the
-- drivers referenced in pico.lst.
--------------------------------------------------

SOUNDS["UPD7759"] = true
SOUNDS["SN76496"] = true
SOUNDS["YM2154"] = true


--------------------------------------------------
-- specify available video cores
--------------------------------------------------

VIDEOS["SEGA315_5313"] = true


--------------------------------------------------
-- specify available machine cores
--------------------------------------------------

MACHINES["INPUT_MERGER"] = true


--------------------------------------------------
-- specify available bus cores
--------------------------------------------------

BUSES["MEGADRIVE"] = true
BUSES["PICO_PS2"] = true


--------------------------------------------------
-- This is the list of files that are necessary
-- for building all of the drivers referenced
-- in pico.lst
--------------------------------------------------

function createProjects_mame_pico(_target, _subtarget)
	project ("mame_pico")
	targetsubdir(_target .."_" .. _subtarget)
	kind (LIBTYPE)
	uuid (os.uuid("drv-mame-pico"))
	addprojectflags()
	precompiledheaders_novs()

	includedirs {
		MAME_DIR .. "src/osd",
		MAME_DIR .. "src/emu",
		MAME_DIR .. "src/devices",
		MAME_DIR .. "src/mame/shared",
		MAME_DIR .. "src/lib",
		MAME_DIR .. "src/lib/util",
		MAME_DIR .. "3rdparty",
		GEN_DIR  .. "mame/layout",
	}

files{
	MAME_DIR .. "src/mame/sega/segapico.cpp",
	MAME_DIR .. "src/mame/sega/megadriv.cpp",
	MAME_DIR .. "src/mame/sega/mdconsole.cpp",
}
end

function linkProjects_mame_pico(_target, _subtarget)
	links {
		"mame_pico",
	}
end
