#!/usr/bin/env python3

from capstone import *
from elftools.elf.elffile import ELFFile
import sys


def is_elf(file_name):
    magic = None
    with open(file_name, "rb") as f:
        raw = f.read()
        magic = raw[:4]
    return (
        magic[0] == 0x7F
        and magic[1] == ord("E")
        and magic[2] == ord("L")
        and magic[3] == ord("F")
    )


def read_section(f, name):
    elffile = ELFFile(f)
    section = elffile.get_section_by_name(name)
    print(section.header)

    addr = section.header["sh_addr"]
    offset = section.header["sh_offset"]
    size = section.header["sh_size"]

    f.seek(offset)
    buf = f.read(size)
    return buf, addr, offset


def get_object_code(file_name, section_name):
    with open(file_name, "rb") as f:
        if is_elf(file_name):
            buf, addr, offset = read_section(f, section_name)
        else:
            raise RuntimeError("Unsupported file format.")

    return buf, addr, offset


def disassemble(file_name, section_name):
    code, entry_point, offset = get_object_code(file_name, section_name)
    md = Cs(CS_ARCH_ARM, CS_MODE_THUMB + CS_MODE_BIG_ENDIAN)
    for i in md.disasm(code, entry_point):
        print("0x%x: %s\t%s\t%s" % (i.address, i.bytes.hex(), i.mnemonic, i.op_str))


if __name__ == "__main__":
    file_name = sys.argv[1]
    disassemble(file_name, ".text:main")
    disassemble(file_name, ".text:hexdump")
