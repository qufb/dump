#!/usr/bin/env python3

import sys

with open(sys.argv[1], "r+b") as f:
    # .dat contents
    f.seek(0x00225560)
    f.write(b"\x00" * (0x002F0000 - 0x00225560))

    # w_hdr_meta table entries
    f.seek(0x06098CA8)
    f.write(
        b"\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        * ((0x060A1608 - 0x06098CA8) // 0x10)
    )

    # VDP1 VRAM
    f.seek(0x05C00000)
    f.write(b"\x00" * 0x80000)
