// Apply function names present in debug info, embedded after each corresponding function body.
//
//@author 
//@category _NEW_
//@keybinding 
//@menupath 
//@toolbar 

import ghidra.app.script.GhidraScript;
import ghidra.program.model.mem.*;
import ghidra.program.model.lang.*;
import ghidra.program.model.pcode.*;
import ghidra.program.model.util.*;
import ghidra.program.model.reloc.*;
import ghidra.program.model.data.*;
import ghidra.program.model.block.*;
import ghidra.program.model.symbol.*;
import ghidra.program.model.scalar.*;
import ghidra.program.model.listing.*;
import ghidra.program.model.address.*;
import ghidra.app.script.GhidraScript;
import ghidra.program.model.address.*;
import ghidra.program.flatapi.FlatProgramAPI;
import ghidra.program.disassemble.*;
import ghidra.util.datastruct.ByteArray;
import ghidra.util.task.ConsoleTaskMonitor;
import ghidra.app.util.importer.MessageLog;
import ghidra.app.util.PseudoData;
import ghidra.program.model.mem.DumbMemBufferImpl;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class ApplyFuncNamesFromEmbeddedDebugInfo extends GhidraScript {

	public void run() throws Exception {
		Program program = currentProgram;
		Listing listing = program.getListing();
		Disassembler disassembler = Disassembler
				.getDisassembler(currentProgram, monitor, DisassemblerMessageListener.CONSOLE);

		final String regex = "\\x4E\\x75.[0-9A-Za-z_-]+\\x00";
		final String expectedMnemonic = "rts";
		final int maxFuncLen = 100;
		final int minFuncLen = 4;
		final byte[] buf = new byte[maxFuncLen];
		Address lastAddress = getFirstFunction().getEntryPoint();
		while (lastAddress != null) {
			Address foundAddress = findBytes(lastAddress, regex);
			if (foundAddress == null) {
				break;
			}
			
			String foundAddressHex = toHex(foundAddress.getUnsignedOffset());
			lastAddress = foundAddress.getNewAddress(foundAddress.getUnsignedOffset() + 1);

			Instruction ins = listing.getInstructionAt(foundAddress);
			if (ins == null || ins.getMnemonicString() == null) {
				println(String.format("Skipping: Invalid ins, loc=%s.", foundAddressHex));
				continue;
			}

			if (!expectedMnemonic.equalsIgnoreCase(ins.getMnemonicString())) {
				println(String.format("Skipping: not %s.", expectedMnemonic));
				continue;
			}
			
			Address funcNameAddress = foundAddress.getNewAddress(foundAddress.getUnsignedOffset() + 3);
			currentProgram.getMemory().getBytes(funcNameAddress, buf, 0, maxFuncLen);
			String funcName = "";
			for (byte b : buf) {
				if (b == 0) {
					break;
				}
				funcName += (char)b;
			}
			
			if (funcName.length() < minFuncLen) {
				println(String.format("Skipping: Name is too small (expected > %d, got %d).", minFuncLen, funcName.length()));
				continue;
			}
			
			Function func = getFunctionContaining(foundAddress);
			if (func == null) {
				println(String.format("Skipping: Func not defined for address @ %s).", foundAddressHex));
				continue;
			}
			
			Address funcAddress = func.getEntryPoint();
			String funcAddressHex = toHex(funcAddress.getUnsignedOffset());
			println(String.format("%s @ %s, entry @ %s", funcName, foundAddressHex, funcAddressHex));
			
			try {
				func.setName(funcName, SourceType.ANALYSIS);
			} catch (Exception e) {
				println(String.format("Failed to set function name: %s\n", e.getMessage()));
			}
		}
	}
	
	private String toHex(long v) {
		return String.format("0x%x", v);
	}
}
