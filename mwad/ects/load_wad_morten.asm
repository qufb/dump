.set noreorder

.global __start
.text
__start:
j cave
nop

.section ".text:cave"
cave:
# get WAD index
lui $at,0x800c
addu $at,$at,$s0
lw $s1,-0x3ac4($at)
# is WAD for level 24?
ori $a0, $0, 0x1cc
bne $a0, $s1, cave_end
nop

cave_extra:
# load WAD for Morten
ori $a0, $0, 0x231
jal read_res
nop
ori $a0, $0, 0x231
jal read_res_after
nop

cave_end:
# Execute original instructions patched by trampoline
lui $at,0x800c
addu $at,$at,$s0
# Back to original function
j 0x8004af9c
nop
nop

.section ".text:read_res"
read_res:
jr $ra
nop
.section ".text:read_res_after"
read_res_after:
jr $ra
nop
