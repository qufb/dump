#!/usr/bin/env python3

import sys
import struct
import parse

rom_base = 0x80000000

chksum_offset = 0xc

mask_size_offset = 0x18
mask_size_8mb = 0x00800000
mask_size_new = 0x00400000

rsrc_size = 0x31E8
rsrc_offset = 0x0075291C
rsrc_new_offset = 0x00200000
msgs_ref = 0x178

font_tiles_offset = 0x00746892
font_pal_offset = 0x00748F14
font_tiles_new_offset = 0x00204000
font_pal_new_offset = font_tiles_new_offset + (font_pal_offset - font_tiles_offset)
font_pal_ref = 0x21850
font_tiles_ref = 0x21854

host_rom = sys.argv[1]
with open(host_rom, "rb") as f:
    rom = f.read()

obj_new = sys.argv[2]
with open("patched.bin", "wb") as f:
    f.write(rom[:mask_size_new])

    # Patch-out checksum
    f.seek(chksum_offset)
    f.write(b"\x00" * 4)

    # Set new mask size, maybe not checked...
    f.seek(mask_size_offset)
    f.write(struct.pack(">L", mask_size_new))

    # Copy end pattern to new end offset, maybe not checked...
    end = rom[mask_size_8mb - 0x100 :]
    f.seek(mask_size_new - 0x100)
    f.write(end)

    # Copy resources pointer tables to fit new mask size.
    # Note: Ogg addressing above 4mb, but we won't load them.
    tbl = rom[rsrc_offset : rsrc_offset + rsrc_size]
    f.seek(rsrc_new_offset)
    f.write(tbl)

    # Update references to resources pointer tables:
    # 80000130 49 11           ldr  r1,[80000178] = 8075291c
    # 80000132 4a 12           ldr  r2,[8000017c] = c00cf1e8
    # 80000134 1a 12           sub  r2,r2,r0
    # 80000136 f0 28 fa 53     bl   strncpy
    f.seek(msgs_ref)
    f.write(struct.pack(">L", rom_base + rsrc_new_offset))

    # Update main() to call our function.
    code, entry_point, _ = parse.get_object_code(obj_new, ".text:main")
    offset = entry_point - rom_base
    f.seek(offset)
    f.write(code)
    code, entry_point, _ = parse.get_object_code(obj_new, ".text:hexdump")
    offset = entry_point - rom_base
    f.seek(offset)
    f.write(code)

    # Copy test mode font data to fit new mask size.
    font_data = rom[font_tiles_offset : font_pal_offset + 7 * 2]
    f.seek(font_tiles_new_offset)
    f.write(font_data)

    # Update set_work_print_tiles() to reference new font offsets:
    # 8002183e 48 04           ldr  r0=>data_print_pal,[80021850]
    # 80021840 49 04           ldr  r1=>data_print_tiles,[80021854]
    # 80021842 f0 05 ff 99     bl   match_pal_w_tiles
    f.seek(font_pal_ref)
    f.write(struct.pack(">L", rom_base + font_pal_new_offset))
    f.seek(font_tiles_ref)
    f.write(struct.pack(">L", rom_base + font_tiles_new_offset))

    # Copy .rodata
    code, entry_point, _ = parse.get_object_code(obj_new, ".rodata")
    offset = entry_point - rom_base
    f.seek(offset)
    f.write(code)
