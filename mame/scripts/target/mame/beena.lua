-- license:BSD-3-Clause
-- copyright-holders:MAMEdev Team

---------------------------------------------------------------------------
--
--   beena.lua
--
--   Small driver-specific example makefile
--   Use make SUBTARGET=beena to build
--
---------------------------------------------------------------------------


--------------------------------------------------
-- Specify all the CPU cores necessary for the
-- drivers referenced in beena.lst.
--------------------------------------------------

CPUS["AP2010CPU"] = true
CPUS["ARM7"] = true


--------------------------------------------------
-- Specify all the sound cores necessary for the
-- drivers referenced in beena.lst.
--------------------------------------------------

SOUNDS["AP2010PCM"] = true


--------------------------------------------------
-- specify available video cores
--------------------------------------------------

-- VIDEOS["FOO"] = true


--------------------------------------------------
-- specify available machine cores
--------------------------------------------------

MACHINES["AP2010MIDI"] = true
MACHINES["BOOKLET"] = true
MACHINES["MIDI_STREAM"] = true
MACHINES["STORYWARE"] = true


--------------------------------------------------
-- specify available bus cores
--------------------------------------------------

-- BUSES["FOO"] = true


--------------------------------------------------
-- This is the list of files that are necessary
-- for building all of the drivers referenced
-- in beena.lst
--------------------------------------------------

function createProjects_mame_beena(_target, _subtarget)
	project ("mame_beena")
	targetsubdir(_target .."_" .. _subtarget)
	kind (LIBTYPE)
	uuid (os.uuid("drv-mame-beena"))
	addprojectflags()
	precompiledheaders_novs()

	includedirs {
		MAME_DIR .. "src/osd",
		MAME_DIR .. "src/emu",
		MAME_DIR .. "src/devices",
		MAME_DIR .. "src/mame/shared",
		MAME_DIR .. "src/lib",
		MAME_DIR .. "src/lib/util",
		MAME_DIR .. "3rdparty",
		GEN_DIR  .. "mame/layout",
	}

files{
	MAME_DIR .. "src/mame/sega/sega_beena.cpp",
}
end

function linkProjects_mame_beena(_target, _subtarget)
	links {
		"mame_beena",
	}
end
