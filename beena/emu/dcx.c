#include <unicorn/unicorn.h>
#include <udbserver.h>
#include <stdio.h>
#include <stdlib.h>

#define BEENA_4M_MASK_SIZE 0x400000
#define BEENA_8M_MASK_SIZE 0x800000
#define BIOS_SIZE 0x20000

unsigned char bios[BIOS_SIZE];
unsigned char rom[BEENA_8M_MASK_SIZE];

static bool hook_mem_fetch_unmapped(uc_engine *uc, uint64_t address) {
    return false;
}

static bool hook_mem_invalid(uc_engine *uc, uc_mem_type type, uint64_t address, int size, int64_t value, void *user_data) {
    switch (type) {
    case UC_MEM_READ_UNMAPPED:
        printf("! UC_MEM_READ_UNMAPPED @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_WRITE_UNMAPPED:
        printf("! UC_MEM_WRITE_UNMAPPED @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_FETCH_UNMAPPED:
        printf("! UC_MEM_FETCH_UNMAPPED @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_READ_PROT:
        printf("! UC_MEM_READ_PROT @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_WRITE_PROT:
        printf("! UC_MEM_WRITE_PROT @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_FETCH_PROT:
        printf("! UC_MEM_FETCH_PROT @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    default:
        printf("! UC_MEM @ 0x%" PRIx64 "\n", address);
        return false;
    }
}

static void hook_code(uc_engine *uc, uint64_t address, uint32_t size, void *user_data) {
    printf("> trace @ 0x%" PRIx64 "\n", address, size);
    hook_mem_fetch_unmapped(uc, address);
}

void dcx(uc_engine *uc, char *filename, uint64_t dst, char *dst_filename) {
    unsigned long src = strtol(filename, NULL, 16);
    unsigned long gfx_size = (rom[src + 3]) | (rom[src + 2] << 8) | (rom[src + 1] << 16);
    src += 0x80000000;

    // bios' dcx()
    unsigned long start = 0x62ac;
    unsigned long until = 0x64bc;

    uc_reg_write(uc, UC_ARM_REG_R0, &dst);
    uc_reg_write(uc, UC_ARM_REG_R1, &src);
    uc_emu_start(uc, start, until, 0, 0);

    unsigned char gfx_dcx[gfx_size];
    if (uc_mem_read(uc, dst, &gfx_dcx, gfx_size) != UC_ERR_OK) {
        printf("uc_mem_read fail for address: 0x%" PRIx64 "\n", dst);
    }
    FILE *f = fopen(dst_filename, "wb");
    fwrite(gfx_dcx, gfx_size, 1, f);
    fclose(f);
}

int main(int argc, char *argv[]) {
    if (argc < 4) {
        fprintf(stderr, "Usage: %s bios.bin rom.bin tiles_hex_rom_offset [meta_hex_rom_offset]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    FILE *f = fopen(argv[1], "rb");
    fseek(f, 0, SEEK_END);
    unsigned long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    fread(bios, fsize, 1, f);
    fclose(f);

    f = fopen(argv[2], "rb");
    fseek(f, 0, SEEK_END);
    fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    fread(rom, fsize, 1, f);
    fclose(f);

    uc_engine *uc;
    uc_hook trace1, trace2, trace3;
    uc_open(UC_ARCH_ARM, UC_MODE_BIG_ENDIAN, &uc);

    uc_mem_map(uc, 0x00000000L, 0x20000L, UC_PROT_READ | UC_PROT_EXEC);
    uc_mem_map(uc, 0x20000000L, 0xF0000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x40000000L, 0xF0000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x50000000L, 0xF0000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x60000000L, 0xF0000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x70000000L, 0xF0000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x80000000L, BEENA_8M_MASK_SIZE, UC_PROT_READ | UC_PROT_EXEC);
    uc_mem_map(uc, 0xA0FFC000L, 0x4000L, UC_PROT_ALL);
    uc_mem_map(uc, 0xC0000000L, 0xC0000L, UC_PROT_ALL);
    uc_mem_map(uc, 0xC00C0000L, 0x1F0000L, UC_PROT_ALL);

    uc_mem_write(uc, 0x00000000L, bios, BIOS_SIZE);
    uc_mem_write(uc, 0x80000000L, rom, BEENA_8M_MASK_SIZE);

    // Patch return instruction, just in case...
    const unsigned char loop[4] =  {0xea, 0xff, 0xff, 0xfe};
    uc_mem_write(uc, 0x64c0, loop, 0x4);

    unsigned long sp = 0xC00FFF80L;
    uc_reg_write(uc, UC_ARM_REG_SP, &sp);

    uc_hook_add(uc, &trace1, UC_HOOK_CODE, hook_code, NULL, 1, 0);
    //uc_hook_add(uc, &trace2, UC_HOOK_MEM_READ, hook_mem_read, NULL, 1, 0);
    uc_hook_add(uc, &trace3,
        UC_HOOK_MEM_READ_PROT |
        UC_HOOK_MEM_WRITE_PROT |
        UC_HOOK_MEM_FETCH_PROT |
        UC_HOOK_MEM_READ_UNMAPPED |
        UC_HOOK_MEM_WRITE_UNMAPPED |
        UC_HOOK_MEM_FETCH_UNMAPPED |
        UC_HOOK_MEM_UNMAPPED,
        hook_mem_invalid, NULL, 1, 0);

    //udbserver(uc, 12345, start);

    //                  warn: meta @ 0x8004d2fc; tiles @ 0x8004ce02;
    //                bandai: meta @ 0x8073eb52; tiles @ 0x8073e42e;
    //             copyright: meta @ 0x807401c6; tiles @ 0x8073ec22;
    // testversion_titlelogo: meta @ 0x80745620; tiles @ 0x80740246;
    dcx(uc, argv[3], 0xc0000100, "out.gfx_tiles.bin");
    if (argc > 4) {
        dcx(uc, argv[4], 0xc0100100, "out.gfx_meta.bin");
    }

    return 0;
}
