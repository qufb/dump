#!/usr/bin/env python3

import sys
import struct
import parse

ram_base = 0xc000_0000

chksum_offset = 0xc
reset_offset = 0x100

host_rom = sys.argv[1]
with open(host_rom, "rb") as f:
    rom = f.read()

obj_new = sys.argv[2]
with open("patched.bin", "wb") as f:
    # Copy ROM header
    f.write(rom[:reset_offset])
    # Patch-out checksum
    f.seek(chksum_offset)
    f.write(b"\x00" * 4)

    # 80000100 e5 1f d1 00     ldr        sp,[->SP]
    # 80000104 e5 9f 00 00     ldr        r0,[0x8000010c]
    # 80000108 e1 2f ff 10     bx         r0=>reset
    # 8000010c 80 00 01 10
    f.seek(reset_offset)
    f.write(b"\xe5\x1f\xd1\x00")
    f.write(b"\xe5\x9f\x00\x00")
    f.write(b"\xe1\x2f\xff\x10")
    f.write(b"\xc0\x00\x01\x11")  # thumb mode address of reset()

    # Update main() to call our function.
    code, entry_point, _ = parse.get_object_code(obj_new, ".text:main")
    offset = entry_point - ram_base
    print(hex(offset))
    f.seek(offset)
    f.write(code)
    code, entry_point, _ = parse.get_object_code(obj_new, ".text:hexdump")
    offset = entry_point - ram_base
    print(hex(offset))
    f.seek(offset)
    f.write(code)

    # Copy .rodata
    code, entry_point, _ = parse.get_object_code(obj_new, ".rodata")
    offset = entry_point - ram_base
    print(hex(offset))
    f.seek(offset)
    f.write(code)
