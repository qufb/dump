# DOS / Windows

```
7 CITIES OF GOLD.
ACTUA SOCCER.
AIR BUCKS.
ALIEN LOGIC.
ALPHA WAVES.
AMAZON - GUARDIANS OF EDEN.
ARYA VAIV.
BARBIE SUPER MODEL.
BATMAN FOREVER.
BATTLE ON THE BLACK SEA.
BEYOND ZORK - THE COCONUT OF QUENDOR.
BIG RED ADVENTURE.
BREACH 3.
BRICK LAYER.
CHAMP ASTEROID.
CHAMP CENTIPEDE.
CHICKENS 3.
CISCO HEAT.
CLOCK WISER - TIME IS RUNNING OUT.
CONFLICT KOREA.
COSMIC SPACEHEAD.
CYBER EMPIRE.
DDAY - THE BEGINNING OF THE END.
DESTRUCTION DERBY 2 (Windows PE).
DOMINUS.
EAGLE'S RIDER.
EARTHRISE - A GUILD INVESTIGATION.
ENTREPRENEUR (Windows PE).
EPIC BASEBALL.
EXCELSIOR - PHASE ONE LYSANDIA.
EXPLORA 2.
EXTREME PINBALL (?).
FIRETEAM 2200 (Amiga version also contains debug infos).
GARFIELD - CAUGHT IN THE ACT (Windows PE).
GOLD OF AMERICA - THE CONQUEST OF THE NEW WORLD.
GRAND PRIX MANAGER 2 (Windows NE).
HEIMDALL 2 - INTO THE HALL OF WORLDS.
HORROR ZOMBIES FROM THE CRYPT.
HOYLE CLASSIC GAMES.
IN SEARCH OF DR RIPTIDE.
IRON LORD.
JILL OF THE JUNGLE.
JILL OF THE JUNGLE 3 - JILL SAVES THE PRINCE.
JUDGE DREDD.
KEEF THE THIEF.
KEYS TO MARAMON.
KURTAN RUSSIAN.
LASER CHESS.
LEISURE SUIT LARRY 6 - SHAPE UP OR SLIP OUT.
MAGIC CANDLE 2.
MASQUE BLACKJACK.
MASTER OF ORION II - BATTLE AT ANTARES.
MATCH IT.
MIND BENDER.
MIND SHADOW.
MORTAL KOMBAT II
MOTOR CITY.
NECTARIS.
OPERATION CARNAGE.
OUTLAW.
PAC MANIAC.
PEA SHOOT IN PETE.
PIRATES GOLD (Windows NE).
PLAGUE MOON.
SEA ROGUE.
SKATE ROCK.
SKUNNY - LOST IN SPACE.
SLOB ZONE 3D.
SOVIET.
SPACE HULK.
SPECTRE VR.
SPELLCASTING 301 - SPRING BREAK.
STARTREK COMBAT SIMULATOR.
STOCK MARKET - THE GAME.
STRIKE SQUAD.
SWORDS OF XEEN.
TASS TIMES IN TONE TOWN.
TEAR DOWN THE WALL.
THE BLUE AND THE GRAY.
THIRD MILLENIUM.
TO SERVE AND TO PROTECT.
TRANS LAND.
TV SPORTS BOXING.
ULTIMA 8.
ULTIMATE SOCCER MANAGER.
ULTRIS.
WARGAME CONSTRUCTION SET 2 - TANKS.
WHEN TWO WORLDS WAR.
WILD WORLD OF SPORT BOXING.
WITCH HAVEN.
ZEPPELIN - GIANTS OF THE SKY.
ZORRO.
```

- [Kroah's Game Reverse Engineering Forum &bull; View topic \- A list of 92 DOS games containing debug infos](http://bringerp.free.fr/forum/viewtopic.php?f=1&t=128)

```
Syndicate - MAIN.EXE - 506kb
Syndicate Wars - MAIN.EXE - 1701kb
Dungeon Keeper - keeper.exe - 1879kb
Dungeon Keeper - keeper95.exe - 1387kb
Deeper Dungeons - deeper95.exe - 1432kb
Dungeon Keeper 2 - DKII.exe or DKII.icd - 2908kb
Populous 3 - popTB.exe - 2002kb
Populous 3 - D3DPopTB.exe - 2222kb
Theme Hospital demo - HOSPITAL.EXE - 1674kb
```

> Games compiled using Watcom C++ have debug info inside EXE file, while games compiled with Microsoft VC++ have external .PDB file.

- [Debug versions of other Bullfrog games](https://keeperklan.com/threads/557-Debug-versions-of-other-Bullfrog-games)

# FM Towns

- Symbol table begins with `SYM1` (is it part of Phar Lap executable format?)

# Consoles

- [Ships with Debugging Symbols \- Dolphin Emulator Wiki](https://wiki.dolphin-emu.org/index.php?title=Ships_with_Debugging_Symbols)
- [User:Kojin/PS2 Games With Debug Symbols \- The Cutting Room Floor](https://tcrf.net/User:Kojin/PS2_Games_With_Debug_Symbols)
- [List of video games shipped with debug symbols\. · GitHub](https://gist.github.com/mariomadproductions/db19996c7109a0289847bb0ec69e32e8)
- [symbols](https://www.retroreversing.com/games/symbols)
    - [ps1-debug-symbols](https://www.retroreversing.com/ps1-debug-symbols)
        - [GitHub \- stohrendorf/symdump: Dumper for PSX SYM debug files](https://github.com/stohrendorf/symdump/)
- GameCube: .map files
- Wii: .sel files
