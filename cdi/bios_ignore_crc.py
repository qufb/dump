#!/usr/bin/env python3

"""
Patches a jump instruction in the bios roms to ignore the
CRC checksum result, so that patched CD images can be loaded
without updating their checksums.
"""

with open("cdimono1/cdi200.rom", "r+b") as f:
    f.seek(0xABB6)
    f.write(b"\x60")
with open("cdimono1/cdi220.rom", "r+b") as f:
    f.seek(0xABB6)
    f.write(b"\x60")
with open("cdimono1/cdi220b.rom", "r+b") as f:
    f.seek(0xABAE)
    f.write(b"\x60")
