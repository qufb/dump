#!/usr/bin/env python3

import gdb
import time


def peek(addr):
    gdb.execute(f"x/40wx {hex(addr)}")


def poke(addr, val):
    print(hex(addr), hex(val))
    gdb.execute(f"set *(char**)({hex(addr)}) = {hex(val)}")


def peek_state():
    peek(0x70000000)
    peek(0x70010000)
    peek(0x700105C0)
    peek(0x70020000)


"""
poke(0x7000000C, 0xB9000000)
poke(0x7000000C, 0xB9320100)
poke(0x7000000C, 0xC0000000)
poke(0x7000000C, 0xc6000000)
for i in range(0x80):
for j in range(16):
   poke(0x7000000C, 0xc6000000 + (j * 0x010000))
   for i in range(48, 52, 1):
       poke(0x7000000C, 0x96007f00 + (i * 0x010000))
       poke(0x7000000C, 0x86000000 + (i * 0x010000))
for i in range(48, 52, 1):
    poke(0x7000000C, 0x90007f00 + (i * 0x010000))
    poke(0x7000000C, 0x80000000 + (i * 0x010000))

for i in range(4):
    for j in range(16):
        poke(0x7000000C, 0xB0000000 + (i * 0x0100))
        poke(0x7000000C, 0xB0320000 + (j * 0x0100))
        poke(0x7000000C, 0xC0010000)
        peek_state()

# crash cymbal
poke(0x7000000C, 0x99317f00)
poke(0x7000000C, 0x89310000)

# dump percurssion
for i in range(0x80):
    poke(0x7000000C, 0x99007f00 + (i * 0x010000))
    poke(0x7000000C, 0x89000000 + (i * 0x010000))

# dump patches
for i in range(0x80):
    poke(0x7000000C, 0xC0000000 + (i * 0x010000))
    poke(0x7000000C, 0x903C7F00)
    peek_state()
    poke(0x7000000C, 0x803C0000)

# dump channel defaults
for i in range(0x10):
    poke(0x7000000C, 0x903C7F00 + (i * 0x01000000))
    peek_state()
    gdb.execute(f"dump memory ~/20230604/change_{hex(i)}.0x70010000_0x70011000.sorei.midi.mem 0x70010000 0x70011000")
    poke(0x7000000C, 0x803C0000 + (i * 0x01000000))

# dump program changes
for i in range(0x80):
    v = 0xC0000000 + (i * 0x00010000)
    poke(0x7000000C, v)
    gdb.execute(
        f"dump memory ~/20230608_1700/{hex(v)}.0x70010000_0x70011000.mem 0x70010000 0x70011000"
    )
for i in range(0x20, 0x40, 1):
    v = 0xC9000000 + (i * 0x00010000)
    poke(0x7000000C, v)
    gdb.execute(
        f"dump memory ~/20230608_1700/{hex(v)}.0x70010000_0x70011000.mem 0x70010000 0x70011000"
    )

# hbreak *0xc000046c
i = 0
while i < 4:
    gdb.execute("c")
    pc = int(str(gdb.parse_and_eval("$pc")).split()[0], 16)
    if pc == 0xc000046c:
        poke(0x7000000C, 0x903C7F00 + (i * 0x00000100))
        time.sleep(1)
        poke(0x7000000C, 0x803C0000)
        i += 1
"""

for i in range(0x80):
    v = 0xC2000000 + (i * 0x00010000)
    poke(0x7000000C, v)
    gdb.execute(
        f"dump memory ~/20230608_1700/{hex(v)}.0x70010000_0x70011000.mem 0x70010000 0x70011000"
    )
