#!/usr/bin/env python3

from PIL import Image
import numpy as np
import sys


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


bios_filename = sys.argv[1]
nocart_tiles_filename = sys.argv[2]
nocart_tilemap_filename = sys.argv[3]
with open(bios_filename, "rb") as f:
    bios_bytes = f.read()
with open(nocart_tiles_filename, "rb") as f:
    nocart_tiles = f.read()
with open(nocart_tilemap_filename, "rb") as f:
    nocart_tilemap = f.read()

w = nocart_tiles[0]
h = nocart_tiles[1]
TILE_LEN = 16

PAL_OFFSET = 0xDD8C

rgb_colors = []
num_colors = int.from_bytes(bios_bytes[PAL_OFFSET : PAL_OFFSET + 2], byteorder="big")
colors = bios_bytes[PAL_OFFSET + 2 : PAL_OFFSET + 2 + num_colors * 2]

rgb_colors.append([255, 255, 255])
for i in range(0, 2 * num_colors, 2):
    color = colors[i : i + 2]
    color = int.from_bytes(color, byteorder="big")
    b = (color >> 10) & 0b11111
    g = (color >> 5) & 0b11111
    r = color & 0b11111
    rgb = [int(x * (255 / 31)) for x in [r, g, b]]
    rgb_colors.append(rgb)

rgb_pixels = []
tiles_count = w * h
tile_size = TILE_LEN * TILE_LEN
tile_data = list(chunks(nocart_tiles[2:], 2))
for transform, i in tile_data:
    tile_offset = tile_size * (i - 1)
    for y in range(TILE_LEN):
        if transform == 0x10:
            x_range = range(TILE_LEN - 1, -1, -1)
        else:
            x_range = range(0, TILE_LEN, 1)
        for x in x_range:
            pal_i = nocart_tilemap[tile_offset + (y * TILE_LEN) + x]
            rgb_pixels.append(rgb_colors[pal_i])

rgb_tiles = list(chunks(rgb_pixels, TILE_LEN * TILE_LEN))

idata = np.full(
    ((len(rgb_tiles) // w) * TILE_LEN, w * TILE_LEN, 3), 0xFF, dtype=np.uint8
)
tdata = rgb_tiles
for ti in range(len(rgb_tiles)):
    wi = ti % w
    hi = ti // w
    for i in range(0, TILE_LEN):
        for j in range(0, TILE_LEN):
            v = tdata[ti][i + (j * TILE_LEN)]
            idata[(hi * TILE_LEN) + j, (wi * TILE_LEN) + i] = tuple(v)
img = Image.fromarray(idata, "RGB")
img.show()
img.save("out.nocart.png")
