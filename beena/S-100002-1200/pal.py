#!/usr/bin/env python3

from PIL import Image
import math
import numpy as np


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


colors = b"\x00\xff\x10\x86\x21\x0a\x39\xd0\x52\x96"
colors = b'\x00\xff\x10\x86\x21\x0a\x39\xd0\x00\x00\x21\x18\x21\x1f\x21\x9c\x22\x1f\x43\x1f\x13\x1c\x23\x9f\x00\x00\x22\x08\x22\x88\x7f\xff\x33\x0c\x61\x84\x72\x04\x7a\x88\x00\x00\x7f\x10\x33\x8e\x62\x1e\x51\x98\x63\x1f\x52\x9c\x21\x90'
rgb_colors = []
for i in range(0, len(colors), 2):
    color = colors[i : i + 2]
    color = int.from_bytes(color, byteorder="big")
    if color == 0x00ff:
        rgb = [255, 255, 255]
    else:
        b = (color >> 10) & 0b11111
        g = (color >> 5) & 0b11111
        r = color & 0b11111
        rgb = [int(x * (255 / 31)) for x in [r, g, b]]
    rgb_colors.append(rgb)
for rgb_color in rgb_colors:
    for x in rgb_color:
        print(hex(x)[2:].zfill(2), end='')
print()
#print(rgb_colors)

w = 16
TILE_LEN = 8
rgb_pixels = []
for i in rgb_colors:
    for j in range(8 * 8):
        rgb_pixels.append(i)
rgb_tiles = list(chunks(rgb_pixels, 8 * 8))
#print(rgb_tiles)

idata = np.zeros(
    (((len(rgb_tiles) // w) + 1) * TILE_LEN, w * TILE_LEN, 3), dtype=np.uint8
)
tdata = rgb_tiles
for ti in range(len(rgb_tiles)):
    wi = ti % w
    hi = ti // w
    for i in range(0, TILE_LEN):
        for j in range(0, TILE_LEN):
            v = tdata[ti][i + (j * TILE_LEN)]
            idata[(hi * TILE_LEN) + j, (wi * TILE_LEN) + i] = tuple(v)
img = Image.fromarray(idata, "RGB")
img.show()
img.save(f"out.pal.png")
