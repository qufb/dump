#include "stdio.h"

#define DUMP_OFFSET 0x70000000
#define DUMP_LENGTH 0x180

__attribute__((section (".text:set_work_print_tiles")))
void set_work_print_tiles() {}

__attribute__((section (".text:print")))
void print() {}

__attribute__((section (".text:writes_to_40000010_toggle_1")))
void writes_to_40000010_toggle_1(int i) {}

__attribute__((section (".text:writes_to_40000010_toggle_2")))
void writes_to_40000010_toggle_2(int i) {}

__attribute__((section (".text:init_work_print_buf")))
void init_work_print_buf(int i, int x, int y) {}

__attribute__((section (".text:init_io")))
void init_io(void *main_oggs) {}

__attribute__((section (".text:writes_work_default_pal")))
void writes_work_default_pal(unsigned short pal, unsigned int i) {}

__attribute__((section (".text:do_callbacks")))
void do_callbacks() {}

__attribute__((section (".text:hexdump")))
void hexdump() {
    set_work_print_tiles();
    writes_to_40000010_toggle_2(1);
    writes_work_default_pal(0x4210, 0);
    writes_to_40000010_toggle_1(1);
    int vblank_count = 0;
    for (;;) {
        if (vblank_count == 10) {
            init_work_print_buf(0, 2, 2);
            print("70000000\n");
            for (int i = 0x0; i < 0x20; i += 0x10) {
                for (int j = 0; j < 0x10; j++) {
                    if (j > 0 && (j % 4) == 0) {
                        print(" ");
                    }
                    print("%02x", *(unsigned char*)(DUMP_OFFSET + i + j));
                }
                print("\n");
            }
            print("70010000\n");
            for (int i = 0x0; i < 0x80; i += 0x10) {
                for (int j = 0; j < 0x10; j++) {
                    if (j > 0 && (j % 4) == 0) {
                        print(" ");
                    }
                    print("%02x", *(unsigned char*)(DUMP_OFFSET + 0x10000 + i + j));
                }
                print("\n");
            }
            print("70010580\n");
            for (int i = 0x0; i < 0x80; i += 0x10) {
                for (int j = 0; j < 0x10; j++) {
                    if (j > 0 && (j % 4) == 0) {
                        print(" ");
                    }
                    print("%02x", *(unsigned char*)(DUMP_OFFSET + 0x10580 + i + j));
                }
                print("\n");
            }
            print("70010fe0\n");
            for (int i = 0x0; i < 0x20; i += 0x10) {
                for (int j = 0; j < 0x10; j++) {
                    if (j > 0 && (j % 4) == 0) {
                        print(" ");
                    }
                    print("%02x", *(unsigned char*)(DUMP_OFFSET + 0x10fe0 + i + j));
                }
                print("\n");
            }
            vblank_count = 0;
        }
        do_callbacks();
        vblank_count++;
    }
}

__attribute__((section (".text:main")))
int main() {
    init_io(NULL);
    hexdump();
    __asm__("mov r7, r7"); // Pad 2 bytes to avoid bad instruction at 0x80000f5a, maybe unneeded...
    return 0;
}
