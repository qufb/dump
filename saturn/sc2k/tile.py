#!/usr/bin/env python3

from PIL import Image
from pathlib import Path
import struct
import numpy as np
import sys


def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


def parse_pal(data):
    pal = []
    for chunk in chunks(data, 3):
        pal.append([chunk[0], chunk[1], chunk[2]])
    return pal


def parse_main_pal(data):
    pal = []
    for chunk in chunks(data, 4):
        pal.append([chunk[3], chunk[2], chunk[1]])
    return pal


TILE_LEN = 2
pal_name = sys.argv[1]
chunk_name = sys.argv[2]

# 0x290 0x22bbe3 32 bridge
# 0x293 0x22c10d 32
# 0x296 0x22c8ea 32
# 0x299 0x22d017 32
# 0x4c7 0x2c5b2b 32 water
# => 0x418 0x291707
# => 0x41e 0x294dbb

p = 0x06098CA8
tile_offs = {}
tile_pals = {}
with open(chunk_name, "rb") as f:
    for i in range(0x800):
        f.seek(p)
        w = struct.unpack(">B", f.read(1))[0] * 8
        p += 1

        f.seek(p)
        h = struct.unpack(">B", f.read(1))[0]
        p += 1 + 2

        f.seek(p)
        tile_pal = struct.unpack(">B", f.read(1))[0]
        p += 4

        f.seek(p)
        src = struct.unpack(">L", f.read(4))[0]
        print(hex(i), hex(src), w, tile_pal)
        tile_offs[src] = w
        tile_pals[src] = tile_pal
        p += 4 + 4

chunk_dir = Path("chunks")
chunk_dir.mkdir(parents=True, exist_ok=True)
out_dir = Path("out")
out_dir.mkdir(parents=True, exist_ok=True)

with open(pal_name, "rb") as f:
    f.seek(0x13C0)
    pal = parse_main_pal(f.read(0x2000))

child_idx = 0
children = list(chunk_dir.iterdir())
children.sort(key=lambda x: x.name)
for child in children:
    with child.open("rb") as f:
        input_bytes = f.read()

    src = int(child.name, 16)
    if src in tile_offs:
        src_w = tile_offs[src]
    if src_w == 0:
        src_w = 32
    if src in tile_pals:
        tile_pal = tile_pals[src]
    else:
        tile_pal = 0
    w = src_w
    h = len(input_bytes) // w

    rgb_data = []
    for pal_i in input_bytes:
        rgb_data.append(pal[(tile_pal * 0x100) + pal_i])

    idata = np.full(((h + 1) * TILE_LEN, w * TILE_LEN, 3), 0xFF, dtype=np.uint8)
    for ti in range(len(rgb_data)):
        wi = ti % w
        hi = ti // w
        for i in range(0, TILE_LEN):
            for j in range(0, TILE_LEN):
                v = rgb_data[ti]
                idata[(hi * TILE_LEN) + j, (wi * TILE_LEN) + i] = tuple(v)

    child_name = "{:04d}_{:08X}".format(child_idx, src)
    img = Image.fromarray(idata, "RGB")
    # img.show()
    img.save(Path(out_dir / f"{child_name}.png").absolute())
    child_idx += 1
