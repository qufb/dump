#!/usr/bin/env python3

import argparse
import os
import struct


def parse_addr(f, is_verbose, addr, addrs):
    addrs.append(addr)

    if addr == 0:
        return False, addrs, "null"

    if addr >= 0xFF0000:
        # TODO: Needs emulation to figure out RAM contents
        return is_verbose, addrs, "?"

    f.seek(addr)
    first2 = struct.unpack(">H", f.read(0x2))[0]
    if first2 == 0x46FC:
        # Ignore stubbed handlers, e.g.
        # ```
        # 0000035a 46 fc 27 00     move       #0x2700,SR
        # 0000035e 4e 71           nop
        # 00000360 60 fe           bra.b      LAB_00000360
        # ```
        _, first2 = struct.unpack(">HH", f.read(0x4))

    while first2 == 0x4E71:
        # found `nop`, skip to next instruction
        first2 = struct.unpack(">H", f.read(0x2))[0]

    if first2 == 0x4EF8:
        # `jmp #addr.w`
        return parse_addr(f, is_verbose, struct.unpack(">H", f.read(0x2))[0], addrs)
    elif first2 == 0x4EF9:
        # `jmp #addr.l`
        return parse_addr(f, is_verbose, struct.unpack(">L", f.read(0x4))[0], addrs)

    ignored_opcodes = [
        0x4AFC,  # `illegal`
        0x4E72,  # `stop`
        0x4E73,  # `rte`
        0x60FE,  # `bra.b 0x0` (infinite loop)
    ]
    if first2 in ignored_opcodes:
        return False, addrs, hex(first2)

    return True, addrs, hex(first2)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v",
        "--verbose",
        type=str,
        help="Include all non-stubbed IRQs, not just valid unknown ones",
    )
    parser.add_argument("file", type=str, help="ROM filename")
    args = parser.parse_args()

    with open(args.file, "rb") as f:
        f.seek(0, os.SEEK_END)
        size = f.tell()
        if size < 0x200:
            print("[!] Not a ROM: file length is < 0x200.")
            exit(1)

        is_rom = False
        f.seek(0x100)
        console_name = (
            f.read(0x20).decode("latin-1").replace("\x00", "").replace("\xff", "")
        )
        if not console_name:
            # "SEGA" in Copera's reset handler
            f.seek(0x40E)
            console_name = (
                f.read(0x4).decode("latin-1").replace("\x00", "").replace("\xff", "")
            )
        names = [
            "IMA IKUNOJYUKU",
            "IMA IKUNOUJYUKU",
            "SAMSUNG PICO",
            "SEGA",
        ]
        for name in names:
            if console_name.startswith(name):
                is_rom = True
                break
        if not is_rom:
            print("[!] Not a ROM: No console name was found.")
            exit(1)

        f.seek(0x64)
        irqs = struct.unpack(">LLLLLLL", f.read(0x4 * 7))
        is_verbose = args.verbose
        for i, irq in enumerate(irqs):
            is_valid_irq, irq_addrs, irq_bytes = parse_addr(f, is_verbose, irq, [])
            if not is_valid_irq:
                continue

            irq_no = i + 1
            irq_name = f"IRQ{irq_no}"
            if irq_no == 2:
                irq_name += " (EXT)"
            elif irq_no == 4:
                irq_name += " (HBLANK)"
                if not is_verbose:
                    continue
            elif irq_no == 6:
                irq_name += " (VBLANK)"
                if not is_verbose:
                    continue

            print(
                f"[+] {irq_name} @ {' -> '.join(hex(addr) for addr in irq_addrs)} = {irq_bytes}"
            )
