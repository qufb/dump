#!/usr/bin/env python3

"""
Crops input images down to the same size.
Each crop area is interactively drawn by the user.
Effective crop area is computed to fit all user selected
areas, while preserving the target aspect ratio.

[Optional] Preview cropped images in a tiled layout:
```
montage \
    -tile 4x3 \
    -geometry +2+2 \
    -background white \
    -resize 10% \
    -border 1 \
    -bordercolor red \
    cropped/*.png \
    montage.png
```
"""

from pathlib import Path
from PIL import Image
import argparse
import pygame
import sys

pygame.init()


def display_img(screen, px, topleft, prior):
    if topleft:
        # ensure that the rect always has positive width, height
        x, y = topleft
        width = pygame.mouse.get_pos()[0] - topleft[0]
        height = pygame.mouse.get_pos()[1] - topleft[1]
        if width < 0:
            x += width
            width = abs(width)
        if height < 0:
            y += height
            height = abs(height)
    else:
        x = pygame.mouse.get_pos()[0]
        y = pygame.mouse.get_pos()[1]
        width = 0
        height = 0

    # eliminate redundant drawing cycles (when mouse isn't moving)
    current = x, y, width, height
    if current == prior:
        return current

    screen.blit(px, px.get_rect())
    display_bbox(screen, topleft, x, y, width, height)
    display_crosshair(screen)
    pygame.display.flip()

    return (x, y, width, height)


def display_bbox(screen, topleft, x, y, width, height):
    if topleft:
        im = pygame.Surface((width + 1, height + 1))
        im.fill((128, 128, 128))
        pygame.draw.rect(im, (32, 32, 32), im.get_rect(), 1)
        im.set_alpha(128)
        screen.blit(im, (x, y))


def display_crosshair(screen):
    w, h = pygame.display.get_surface().get_size()
    CROSS_LEN = 2 * max(w, h)
    im = pygame.Surface((CROSS_LEN, CROSS_LEN), pygame.SRCALPHA)
    pygame.draw.line(
        im, (32, 32, 32), (0, CROSS_LEN // 2), (CROSS_LEN, CROSS_LEN // 2), 1
    )
    pygame.draw.line(
        im, (32, 32, 32), (CROSS_LEN // 2, 0), (CROSS_LEN // 2, CROSS_LEN), 1
    )
    im.set_alpha(128)

    x = pygame.mouse.get_pos()[0]
    y = pygame.mouse.get_pos()[1]
    screen.blit(im, (x - CROSS_LEN // 2, y - CROSS_LEN // 2))


def setup(path, scale):
    px = pygame.image.load(path)
    rect = px.get_rect()
    px = pygame.transform.scale(px, [rect.width / scale, rect.height / scale])
    screen = pygame.display.set_mode(px.get_rect()[2:])
    screen.blit(px, px.get_rect())
    pygame.display.flip()

    return screen, px


def main_loop(screen, px):
    clock = pygame.time.Clock()
    topleft = bottomright = prior = None
    n = 0
    while n != 1:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONUP:
                if not topleft:
                    topleft = event.pos
                else:
                    bottomright = event.pos
                    n = 1
        prior = display_img(screen, px, topleft, prior)
        clock.tick(60)

    return topleft + bottomright


def fit(left, upper, right, lower, max_w, max_h, ratio):
    w = right - left
    h = lower - upper

    print("ratio1:", h / w)
    while h / w < ratio - sys.float_info.epsilon:
        is_adjusted = False
        if upper > 0:
            upper -= 1
            is_adjusted = True
        if lower < max_h:
            lower += 1
            is_adjusted = True
        if not is_adjusted:
            break
        w = right - left
        h = lower - upper
    while h / w > ratio - sys.float_info.epsilon:
        is_adjusted = False
        if right < max_w:
            right += 1
            is_adjusted = True
        if left > 0:
            left -= 1
            is_adjusted = True
        if not is_adjusted:
            break
        w = right - left
        h = lower - upper
    print("ratio2:", h / w)

    return left, upper, right, lower


def fit_wh(left, upper, right, lower, max_w, max_h, target_w, target_h):
    w = right - left
    h = lower - upper

    while h < target_h:
        is_adjusted = False
        if h < target_h and upper > 0:
            upper -= 1
            is_adjusted = True
        h = lower - upper
        if h < target_h and lower < max_h:
            lower += 1
            is_adjusted = True
        h = lower - upper
        if not is_adjusted:
            break
    while w < target_w:
        is_adjusted = False
        if w < target_w and right < max_w:
            right += 1
            is_adjusted = True
        w = right - left
        if w < target_w and left > 0:
            left -= 1
            is_adjusted = True
        w = right - left
        if not is_adjusted:
            break

    return left, upper, right, lower


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d",
        "--dir",
        type=str,
        help="Input directory with images to crop (alternative to --files)",
    )
    parser.add_argument(
        "-f",
        "--files",
        type=str,
        nargs="+",
        help="Input image filenames (alternative to --dir)",
    )
    parser.add_argument(
        "-r",
        "--ratio",
        default=1.4,
        type=float,
        help="Target aspect ratio of cropped image",
    )
    parser.add_argument(
        "-s",
        "--scale",
        default=7,
        type=int,
        help="Preview downscale factor",
    )
    args = parser.parse_args()

    OUT = "cropped"
    files = (
        [f for f in Path(args.dir).iterdir() if f.is_file()] if args.dir else args.files
    )
    basedir = Path(files[0]).parent.resolve()
    (basedir / OUT).mkdir(parents=True, exist_ok=True)

    best_w = 0
    best_h = 0
    crops = {}
    for f in files:
        input_loc = f
        screen, px = setup(input_loc, args.scale)
        rect = px.get_rect()

        left, upper, right, lower = main_loop(screen, px)

        # ensure output rect always has positive width, height
        if right < left:
            left, right = right, left
        if lower < upper:
            lower, upper = upper, lower

        left = left * args.scale
        upper = upper * args.scale
        right = right * args.scale
        lower = lower * args.scale
        max_w = rect.width * args.scale
        max_h = rect.height * args.scale

        crops[f] = {"left": left, "upper": upper, "right": right, "lower": lower}

        w = right - left
        if w > best_w:
            best_w = w
        h = lower - upper
        if h > best_h:
            best_h = h
        print("curr fit:", w, h)
        print("best fit:", best_w, best_h)

    best_left, best_upper, best_right, best_lower = fit(
        0, 0, best_w, best_h, max_w, max_h, args.ratio
    )
    best_w = best_right - best_left
    best_h = best_lower - best_upper
    print("best ratio fit:", best_left, best_upper, best_right, best_lower)

    for f, dims in crops.items():
        left, upper, right, lower = fit_wh(
            dims["left"],
            dims["upper"],
            dims["right"],
            dims["lower"],
            max_w,
            max_h,
            best_w,
            best_h,
        )
        print("crop:", left, upper, right, lower)

        im = Image.open(f)
        im = im.crop((left, upper, right, lower))
        im.save(str(basedir / f"{OUT}/{Path(f).name}"))

    pygame.display.quit()
