-- license:BSD-3-Clause
-- copyright-holders:MAMEdev Team

---------------------------------------------------------------------------
--
--   casio.lua
--
--   Small driver-specific example makefile
--   Use make SUBTARGET=casio to build
--
---------------------------------------------------------------------------


--------------------------------------------------
-- Specify all the CPU cores necessary for the
-- drivers referenced in casio.lst.
--------------------------------------------------

CPUS["HCD62121"] = true


--------------------------------------------------
-- Specify all the sound cores necessary for the
-- drivers referenced in casio.lst.
--------------------------------------------------

-- SOUNDS["YM2154"] = true


--------------------------------------------------
-- specify available video cores
--------------------------------------------------

-- VIDEOS["SEGA315_5313"] = true


--------------------------------------------------
-- specify available machine cores
--------------------------------------------------

-- MACHINES["INPUT_MERGER"] = true


--------------------------------------------------
-- specify available bus cores
--------------------------------------------------

-- BUSES["MEGADRIVE"] = true


--------------------------------------------------
-- This is the list of files that are necessary
-- for building all of the drivers referenced
-- in casio.lst
--------------------------------------------------

function createProjects_mame_casio(_target, _subtarget)
	project ("mame_casio")
	targetsubdir(_target .."_" .. _subtarget)
	kind (LIBTYPE)
	uuid (os.uuid("drv-mame-casio"))
	addprojectflags()
	precompiledheaders_novs()

	includedirs {
		MAME_DIR .. "src/osd",
		MAME_DIR .. "src/emu",
		MAME_DIR .. "src/devices",
		MAME_DIR .. "src/mame/shared",
		MAME_DIR .. "src/lib",
		MAME_DIR .. "src/lib/util",
		MAME_DIR .. "3rdparty",
		GEN_DIR  .. "mame/layout",
	}

files{
	MAME_DIR .. "src/mame/casio/cfx9850.cpp",
}
end

function linkProjects_mame_casio(_target, _subtarget)
	links {
		"mame_casio",
	}
end
