#!/usr/bin/env python3

import os
import sys

LOG = bool(os.environ.get("LOG"))


def log(text):
    if LOG:
        print(text)


def dcx(dst, src):
    dcx_meta = src[0]
    expected_chksum = dcx_meta * 0x10000000 & 0xFFFFFFFF
    actual_chksum = (src[1] + src[2] + src[3] + -1) * 0x10000000 & 0xFFFFFFFF
    if expected_chksum != actual_chksum:
        return 0

    di = 0
    si = 4
    dcx_len = src[1] * 0x10000 + src[2] * 0x100 + src[3]
    dcx_type = dcx_meta & 0x70
    if dcx_type == 0:
        # Uncompressed (direct copy of all bytes)
        i = 0
        while i < dcx_len:
            dst[i] = src[si + i]
            i = i + 1
    elif dcx_type == 0x10:
        uVar5 = src[si]
        si = 5
        i = 0x80
        while di < dcx_len:
            b_src = src[si]
            if (uVar5 & i) == 0:
                # Direct copy
                pbVar1 = si + 1
                pbVar3 = di + 1
                dst[di] = b_src
                log(f"1 [{di:02x}]={b_src:02x}")
            else:
                # Repeat from distance
                pbVar1 = si + 2
                rep_count = (b_src & 0xF) + 2
                pbVar2 = di + (-1 - (src[si + 1] + (b_src & 0xF0) * 0x10))
                while rep_count > -1:
                    pbVar3 = di + 1
                    dst[di] = dst[pbVar2]
                    log(f"2 [{di:02x}]={dst[pbVar2]:02x}")
                    di = pbVar3
                    pbVar2 = pbVar2 + 1
                    rep_count -= 1
            i = i >> 1
            if i == 0:
                i = 0x80
                si = pbVar1 + 1
                uVar5 = src[pbVar1]
            else:
                si = pbVar1
            di = pbVar3
    elif dcx_type == 0x20:
        uVar5 = src[si]
        si += 5
        i = 0x80
        while di < dcx_len:
            if (uVar5 & i) == 0:
                dst[di] = src[si]
                pbVar3 = si + 2
                dst[di + 1] = src[si + 1]
            else:
                b_src = src[si]
                pbVar3 = si + 1
                if b_src < 0x80:
                    bVar7 = 0
                else:
                    bVar7 = 0xFF
                dst[di] = bVar7
                dst[di + 1] = b_src
            i = i >> 1
            si = pbVar3
            if i == 0:
                i = 0x80
                si = pbVar3 + 1
                uVar5 = src[pbVar3]
            di = di + 2
    else:
        if dcx_type != 0x30:
            return 0
        iVar6 = 0
        while True:
            pbVar1 = di + iVar6
            pbVar3 = si
            while True:
                pbVar4 = pbVar3 + 1
                b_src = src[pbVar3]
                i = b_src >> 2
                bVar7 = b_src & 3
                pbVar2 = pbVar1
                if (b_src & 3) == 0:
                    while True:
                        bVar8 = i != 0
                        i = i - 1
                        pbVar1 = pbVar2 + 2
                        dst[pbVar2] = src[pbVar4]
                        pbVar2 = pbVar1
                        si = pbVar4 + 1
                        pbVar4 = pbVar4 + 1
                        if not bVar8:
                            break
                else:
                    si = pbVar4
                    if bVar7 == 1:
                        while True:
                            pbVar1 = pbVar2 + 2
                            dst[pbVar2] = 0
                            bVar8 = i != 0
                            i = i - 1
                            pbVar2 = pbVar1
                            if not bVar8:
                                break
                    elif bVar7 == 2:
                        si = pbVar3 + 2
                        b_src = src[pbVar4]
                        pbVar3 = pbVar1
                        while True:
                            pbVar1 = pbVar3 + 2
                            dst[pbVar3] = b_src
                            bVar8 = i != 0
                            i = i - 1
                            pbVar3 = pbVar1
                            if not bVar8:
                                break
                    elif bVar7 == 3:
                        b_src = src[pbVar4]
                        while True:
                            pbVar1 = pbVar2 + 2
                            dst[pbVar2] = b_src
                            bVar8 = i != 0
                            i = i - 1
                            pbVar2 = pbVar1
                            si = pbVar3 + 2
                            b_src = b_src + 1
                            if not bVar8:
                                break
                pbVar3 = si
                if pbVar1 >= dcx_len:
                    break
            iVar6 = iVar6 + 1
            if iVar6 >= 2:
                break

    return dcx_len


def dcx_at_offset(rom, gfx_offset):
    src = rom[gfx_offset:]
    dcx_meta = src[0]
    dcx_type = dcx_meta & 0x70
    log(f"- cpx meta=0x{dcx_meta:02X}, type=0x{dcx_type:02X}")

    expected_len = src[1] * 0x10000 + src[2] * 0x100 + src[3]
    dst = [0] * expected_len
    got_len = dcx(dst, src)
    log(f"- size expected={hex(expected_len)}, got={hex(got_len)}")
    log(f"- dcx head={dst[:4]}")

    return bytes(dst[:got_len])


def dcx_to_file(rom, gfx_offset, out_name):
    log(f"processing {out_name}...")
    with open(out_name, "wb") as f:
        f.write(dcx_at_offset(rom, int(gfx_offset, 0)))


if __name__ == "__main__":
    if len(sys.argv) < 3:
        raise RuntimeError(
            f"Usage: {sys.argv[0]} rom.bin tiles_hex_rom_offset [meta_hex_rom_offset]"
        )

    with open(sys.argv[1], "rb") as f:
        rom = f.read()

    #                  warn: meta @ 0x8004d2fc; tiles @ 0x8004ce02
    #                bandai: meta @ 0x8073eb52; tiles @ 0x8073e42e
    #             copyright: meta @ 0x807401c6; tiles @ 0x8073ec22
    # testversion_titlelogo: meta @ 0x80745620; tiles @ 0x80740246
    dcx_at_offset(rom, sys.argv[2], "out2b.gfx_tiles.bin")
    if len(sys.argv) > 3:
        dcx_at_offset(rom, sys.argv[3], "out2b.gfx_meta.bin")
