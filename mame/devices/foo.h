// license:BSD-3-Clause
// copyright-holders:QUFB

#ifndef MAME_MACHINE_FOO_H
#define MAME_MACHINE_FOO_H

#pragma once

class foo_device : public device_t
{
public:
	foo_device(const machine_config &mconfig, const char *tag, device_t *owner, uint32_t clock = 0);

protected:
	// device_t implementation
	virtual void device_start() override;
	virtual void device_reset() override;
    virtual void device_add_mconfig(machine_config &config) override;

	// optional information overrides
    virtual ioport_constructor device_input_ports() const override;
};

DECLARE_DEVICE_TYPE(FOO, foo_device)

#endif // MAME_MACHINE_FOO_H;
