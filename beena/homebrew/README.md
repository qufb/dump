# Beena homebrew

The following payloads are available:

### dump

Boots directly into test mode, printing to the screen a hex dump of a hardcoded memory range.

* Takes a 8mb rom (_Soreike! Anpanman Hajimete Kaketa yo! Oboeta yo! Hiragana Katakana: Gojuuon Board Kinou-tsuki_) as base to generate a 4mb rom. This was due to current 4mb dumps not using a printf based test mode (likely the whole text message is hardcoded in tile data).
* Updates the mask size in the rom header.
* Adds footer pattern `\x5a\xa5\xa5\x5a` to the end of the rom (likely pairs with the header pattern `\xa5\x5a\x5a\xa5`).
* Calls the same hardware initialization functions before entering `main()`, although some resources had to be relocated into the 4mb range, along with fixing their references in code (see ./prepare.py).
* `main()` calls `init_io()` with a null pointer instead of an Ogg file pointer (exisiting code already handles both cases), then calls our `hexdump()` (see ./dump.c).
* After loading and printing the hardcoded memory range, `hexdump()` stays in a loop calling `do_callbacks()`, just like the original test mode.

### dump_from_bitmap

Similar to `dump`, but our code is placed under the direct bitmap RAM address at `0xc000_0000`. Instead of patching the base ROM, this payload only calls functions defined in the base ROM, which is expected to be inserted in the console. This allows some arbitrary code to be run, as long as it fits under `0xc6000` bytes (i.e. the full size of the direct bitmap).

The payload can be loaded with the following GDB commands:

```
hbreak *0x80000f40  # Base ROM's main() function entry
c  # Run up to breakpoint

restore patched.bin binary 0xc0000000
set $pc = 0xc0000152  # Patched ROM's main() function entry
set *(char**)(0x40000010) = 0x203  # Set 1x video mode + tile layers visible
c
```

## Build

```sh
# assemble
arm-none-eabi-gcc -O0 -march=armv4t -mthumb -mbig-endian --specs=nosys.specs -c dump.c \
    && arm-none-eabi-ld -EB -o linked.o dump.o -T dump.ld

# disassemble
arm-none-eabi-objdump -d linked.o
arm-none-eabi-objdump -d -s -j .rodata linked.o

# patch base rom
python -m dump.prepare base.bin dump/linked.o
srec_cat patched.bin -binary -byte-swap 2 -o swapped.bin -binary

# emulate
env LD_LIBRARY_PATH="$HOME/.local/lib:$HOME/.local/lib/python3.9/site-packages/unicorn-2.0.0-py3.9-linux-x86_64.egg/unicorn/lib" gcc \
    -L"$HOME/.local/lib" \
    -L"$HOME/.local/lib/python3.9/site-packages/unicorn-2.0.0-py3.9-linux-x86_64.egg/unicorn/lib" \
    -I"$HOME/.local/include" \
    -I"$HOME/.local/lib/python3.9/site-packages/unicorn-2.0.0-py3.9-linux-x86_64.egg/unicorn/include" \
    emu.c -o emu -lunicorn -ludbserver -lpthread -lm
env RUST_BACKTRACE=full LD_LIBRARY_PATH="$HOME/.local/lib:$HOME/.local/lib/python3.9/site-packages/unicorn-2.0.0-py3.9-linux-x86_64.egg/unicorn/lib" ./emu ../homebrew/patched.bin

## test mode
gdb-multiarch \
    -ex='set architecture armv4t' \
    -ex='set arm fallback-mode thumb' \
    -ex='set arm force-mode thumb' \
    -ex='set endian big' \
    -ex='target remote localhost:12345' \
    -ex='dashboard -layout assembly !breakpoints !expressions !history memory registers source stack !threads variables' \
    -ex='dashboard memory watch 0xc00ffe7c 16' \
    -ex='b *0x8002b898' \
    -ex='b *0x80020b7c' \
    -ex='c'

## page parsing
gdb-multiarch \
    -ex='set architecture armv4t' \
    -ex='set arm fallback-mode thumb' \
    -ex='set arm force-mode thumb' \
    -ex='set endian big' \
    -ex='target remote localhost:12345' \
    -ex='b *0x80029396' \
    -ex='b *0x80029416' \
    -ex='dashboard -layout assembly !breakpoints !expressions !history memory registers source stack !threads variables' \
    -ex='dashboard memory watch 0xc00d5448 0x20' \
    -ex='set *(char**)(0x50020034) = 0x000001ff' \
    -ex='set $pc=0x80029388+1'
```
