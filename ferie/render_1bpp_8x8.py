#!/usr/bin/env python3

from PIL import Image
import math
import numpy as np
import pprint
import struct
import sys

TILE_LEN = 8
TILE_BPP = 1
A_PAL = [
    [0xFF, 0xFF, 0xFF],
    [0x00, 0x00, 0x00],
]


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


def tfill(idata, tdata, pal, wi, hi, ti):
    for i in range(0, TILE_LEN):
        for j in range(0, TILE_LEN):
            v = tdata[(ti * TILE_LEN * TILE_LEN) + (i + (j * TILE_LEN))]
            idata[(hi * TILE_LEN) + j, (wi * TILE_LEN) + i] = tuple(pal[v])
    return idata


def render_tiles(input_bytes, tilemap_offset, output_name):
    w = 15
    h = 8
    tile_count = w * h

    iw = w * TILE_LEN
    ih = h * TILE_LEN
    idata = np.zeros((ih, iw, 3), dtype=np.uint8)
    tdata = [0] * TILE_LEN * TILE_LEN * w * h

    rgb_pixels = []
    for j in range(0, h):
        for i in range(0, w):
            tilemap_i = (
                int(
                    struct.unpack(
                        "<h", input_bytes[tilemap_offset : tilemap_offset + 2]
                    )[0]
                )
                & 0xFFFF
            )
            tile_i = (tilemap_offset & 0xF0000) + tilemap_i * TILE_LEN
            print(hex(tilemap_offset), hex(tile_i), hex(tilemap_i))
            for tile8_i in range(TILE_LEN):
                tile8 = int.from_bytes(
                    input_bytes[tile_i + tile8_i : tile_i + tile8_i + 1],
                    byteorder="little",
                    signed=False,
                )
                for bit_i in range(TILE_LEN - 1, -1, -1):
                    bit = (tile8 >> bit_i) & 1
                    rgb_pixels.append(A_PAL[bit])
            tilemap_offset += 2

    tdata = list(chunks(rgb_pixels, TILE_LEN * TILE_LEN))
    for ti in range(len(tdata)):
        wi = ti % w
        hi = ti // w
        for i in range(0, TILE_LEN):
            for j in range(0, TILE_LEN):
                v = tdata[ti][i + (j * TILE_LEN)]
                idata[(hi * TILE_LEN) + j, (wi * TILE_LEN) + i] = tuple(v)

    img = Image.fromarray(idata, "RGB")
    img.show()
    img.save(f"{output_name}.png")


if __name__ == "__main__":
    input_name = sys.argv[1]
    tilemap_offset = int(sys.argv[2], 0)
    with open(input_name, "rb") as f:
        input_bytes = f.read()

    render_tiles(input_bytes, tilemap_offset, hex(tilemap_offset))
