#!/usr/bin/env python3

from pathlib import Path
# from skimage import segmentation
import argparse
import cv2
import numpy as np
import sys


def gamma_correction(image):
    inBlack = np.array([127, 127, 127], dtype=np.float32)
    inWhite = np.array([255, 255, 255], dtype=np.float32)
    inGamma = np.array([1.0, 1.0, 1.0], dtype=np.float32)
    outBlack = np.array([0, 0, 0], dtype=np.float32)
    outWhite = np.array([255, 255, 255], dtype=np.float32)

    new_image = np.clip((image - inBlack) / (inWhite - inBlack), 0, 255)
    new_image = (image ** (1 / inGamma)) * (outWhite - outBlack) + outBlack
    new_image = np.clip(image, 0, 255).astype(np.uint8)
    return new_image


def ratio_adjust(x, y, w, h, max_w, max_h):
    ratio = 15 / 11
    effective_w = w
    while h / effective_w > ratio - sys.float_info.epsilon:
        is_adjusted = False
        if w < max_w - delta:
            w += 2
            effective_w += 2
            is_adjusted = True
        if x > delta:
            x -= 1
            if is_adjusted:
                effective_w -= 1
            else:
                effective_w += 1
            is_adjusted = True
        if not is_adjusted:
            break
    return x, y, w, h


def max_saturation(image, basedir, args, satur_max=80):
    # Set minimum and maximum HSV values to display
    lower = np.array([0, 0, 0])
    upper = np.array([255, satur_max, 255])

    # Convert to HSV format and color threshold
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower, upper)
    saturation_threshold = cv2.bitwise_and(image, image, mask=mask)

    if not args.debug:
        cv2.imwrite(str(basedir / "auto/debug/satur.png"), saturation_threshold)

    return saturation_threshold


def resize(image, scale):
    new_w = 352 * scale
    new_h = 480 * scale
    new_dim = (new_w, new_h)
    return cv2.resize(image, new_dim, interpolation=cv2.INTER_AREA)


def mask2rgb(mask, mask_color=[255, 255, 255]):
    return cv2.cvtColor(mask, cv2.COLOR_GRAY2RGB)
    # return np.concatenate(
    #     ([mask[..., np.newaxis] * color for color in mask_color]), axis=2
    # )


def debug(image, basedir, args):
    name = "saturation / thresh / mask / opening"
    cv2.namedWindow(name)

    cv2.createTrackbar("thresholds", name, 0, 2, lambda _: None)
    cv2.setTrackbarPos("thresholds", name, 2)
    prev_thresholds = 1

    cv2.createTrackbar("satur_max", name, 0, 255, lambda _: None)
    cv2.setTrackbarPos("satur_max", name, 80)
    prev_satur_max = 0

    cv2.createTrackbar("blur", name, 0, 21, lambda _: None)
    cv2.setTrackbarPos("blur", name, args.blur)
    prev_blur = args.blur

    cv2.createTrackbar("canny_min", name, 0, 80, lambda _: None)
    cv2.setTrackbarPos("canny_min", name, args.canny_min)
    prev_canny_min = args.canny_min

    cv2.createTrackbar("canny_max", name, 0, 180, lambda _: None)
    cv2.setTrackbarPos("canny_max", name, args.canny_max)
    prev_canny_max = args.canny_max

    is_updated = False
    while True:
        thresholds = cv2.getTrackbarPos("thresholds", name)
        satur_max = cv2.getTrackbarPos("satur_max", name)
        args.blur = cv2.getTrackbarPos("blur", name)
        args.canny_min = cv2.getTrackbarPos("canny_min", name)
        args.canny_max = cv2.getTrackbarPos("canny_max", name)

        if thresholds == 0:
            args.thresholds = ["adaptive"]
        elif thresholds == 1:
            args.thresholds = ["saturation", "adaptive"]
        else:
            args.thresholds = ["saturation", "otsu", "adaptive"]

        if (
            prev_thresholds != thresholds
            or prev_satur_max != satur_max
            or prev_blur != args.blur
            or prev_canny_min != args.canny_min
            or prev_canny_max != args.canny_max
        ):
            prev_thresholds = thresholds
            prev_satur_max = satur_max
            prev_blur = args.blur
            prev_canny_min = args.canny_min
            prev_canny_max = args.canny_max
            is_updated = True

        if is_updated:
            contour_approximated, satur, thresh, mask, opening = contour_detection(
                image.copy(), basedir, args
            )

            # concatenated = np.concatenate((thresh, mask, opening), axis=1)
            satur = resize(satur, 1)
            thresh = resize(mask2rgb(thresh), 1)
            mask = resize(mask2rgb(mask), 1)
            opening = resize(mask2rgb(opening), 1)
            concatenated = np.concatenate((satur, thresh, mask, opening), axis=1)

            cv2.imshow(name, concatenated)

            is_updated = False

        if cv2.waitKey(10) & 0xFF == ord("q"):
            break

    cv2.destroyAllWindows()


def contour_detection(image, basedir, args):
    saturation_threshold = image
    if "saturation" in args.thresholds:
        saturation_threshold = max_saturation(image, basedir, parsed_args)

    mask = np.zeros(image.shape, dtype=np.uint8)
    gray = cv2.cvtColor(saturation_threshold, cv2.COLOR_BGR2GRAY)

    if args.blur < 4:
        blur = cv2.GaussianBlur(gray, (3, 3), 0)
    elif args.blur < 8:
        blur = cv2.GaussianBlur(gray, (7, 7), 3)
    elif args.blur < 12:
        blur = cv2.GaussianBlur(gray, (11, 11), 7)
    else:
        blur = cv2.GaussianBlur(gray, (21, 21), 9)

    thresh = cv2.adaptiveThreshold(
        blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 99, 3
    )
    if "otsu" in args.thresholds:
        _, thresh_otsu = cv2.threshold(
            blur, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU
        )
        thresh |= thresh_otsu

    # Find light rounded-corners
    edged = cv2.Canny(image=gray, threshold1=args.canny_min, threshold2=args.canny_max)
    cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    for c in cnts:
        cv2.drawContours(mask, [c], -1, (255, 255, 255), -1)

    # thresh = segmentation.clear_border(thresh)
    # mask = segmentation.clear_border(mask)

    # Find horizontal sections and draw on mask
    for i in [3, 30]:
        horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 1))
        detect_horizontal = cv2.morphologyEx(
            thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=i
        )
        cnts = cv2.findContours(
            detect_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
        )
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        for c in cnts:
            cv2.drawContours(mask, [c], -1, (255, 255, 255), -1)

    # Find vertical sections and draw on mask
    thresh = cv2.dilate(thresh, np.ones((3, 1), np.uint8), iterations=3)
    for i in [3, 30]:
        vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 4))
        detect_vertical = cv2.morphologyEx(
            thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=i
        )
        cnts = cv2.findContours(
            detect_vertical, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
        )
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        for c in cnts:
            cv2.drawContours(mask, [c], -1, (255, 255, 255), -1)

    # Fill text document body
    mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
    close_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    close = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, close_kernel, iterations=3)
    cnts = cv2.findContours(close, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    for c in cnts:
        cv2.drawContours(mask, [c], -1, 255, -1)

    # Perform morph operations to remove noise
    # Find contours and sort for largest contour
    opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, close_kernel, iterations=7)
    cnts = cv2.findContours(opening, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)

    # Perform contour approximation
    c = cnts[0]
    peri = cv2.arcLength(c, True)
    contour_approximated = cv2.approxPolyDP(c, 0.003 * peri, True)
    print("peri:  ", peri)
    print("approx:", "".join(str(contour_approximated).split("\n")))

    if not args.debug:
        cv2.imwrite(str(basedir / "auto/debug/thresh.png"), thresh)
        cv2.imwrite(str(basedir / "auto/debug/mask.png"), mask)
        cv2.imwrite(str(basedir / "auto/debug/opening.png"), opening)

    return contour_approximated, saturation_threshold, thresh, mask, opening


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-a",
        "--all",
        default=True,
        action="store_true",
        help="aggressive thresholding (applies all strategies)",
    )
    parser.add_argument(
        "-b",
        "--blur",
        default=21,
        type=int,
        help="gaussian blur size (less preserves details but is sensitive to noise)",
    )
    parser.add_argument(
        "--canny_min",
        default=20,
        type=int,
        help="Canny edge detection min",
    )
    parser.add_argument(
        "--canny_max",
        default=80,
        type=int,
        help="Canny edge detection max (less is better for low contrast)",
    )
    parser.add_argument(
        "-d",
        "--debug",
        default=False,
        action="store_true",
        help="interactive filter debug",
    )
    parser.add_argument(
        "-p",
        "--padding",
        default=30,
        type=int,
        help="padding added to cropped image",
    )
    parser.add_argument("-f", "--file", type=str, help="image to process")
    parser.add_argument(
        "-r",
        "--ratio",
        default=False,
        action="store_true",
        help="best fit cropped image to page ratio (480/352)",
    )
    parser.add_argument(
        "-s",
        "--scale",
        default=0,
        type=int,
        help="scaling factor respecting page ratio (480/352), 0 skips scaling (only crops image)",
    )
    parser.add_argument(
        "-t",
        "--thresholds",
        type=str,
        nargs="+",
        choices=["saturation", "otsu", "adaptive"],
        help="thresholding strategies",
    )
    parsed_args = parser.parse_args()

    basedir = Path(sys.argv[1]).parent.resolve()
    (basedir / "auto/cropped").mkdir(parents=True, exist_ok=True)
    (basedir / "auto/debug").mkdir(parents=True, exist_ok=True)

    filename = parsed_args.file
    image = cv2.imread(filename)

    if parsed_args.all:
        parsed_args.thresholds = ["saturation", "otsu", "adaptive"]
    if parsed_args.debug:
        debug(image, basedir, parsed_args)
        exit(0)

    max_h, max_w, _ = image.shape
    print("coords max:  ", 0, 0, max_w, max_h)

    contour_approximated, _, _, _, _ = contour_detection(
        image.copy(), basedir, parsed_args
    )

    x, y, w, h = cv2.boundingRect(contour_approximated)
    print("coords bbox: ", x, y, w, h)

    delta = parsed_args.padding
    x = max(0, x - delta)
    y = max(0, y - delta)
    h = min(h + delta * 2, max_h - y)
    w = min(w + delta * 2, max_w - x)
    print("coords delta:", x, y, w, h)

    if parsed_args.ratio:
        x, y, w, h = ratio_adjust(x, y, w, h, max_w, max_h)

    out = image[y : y + h, x : x + w]
    print("coords ratio:", x, y, w, h)

    if parsed_args.scale != 0:
        out = resize(out, parsed_args.scale)

    cv2.imwrite(str(basedir / f"auto/cropped/{filename}"), out)
