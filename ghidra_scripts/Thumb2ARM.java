//Workaround for Ghidra incorrectly disassembling Thumb mode instructions from data addresses 
//that don't have the least significant bit set. No idea why it would do that...
//
//@author 
//@category _NEW_
//@keybinding 
//@menupath 
//@toolbar 

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import ghidra.app.cmd.disassemble.ArmDisassembleCommand;
import ghidra.app.script.GhidraScript;
import ghidra.app.util.bin.BinaryReader;
import ghidra.app.util.bin.ByteProvider;
import ghidra.app.util.bin.MemoryByteProvider;
import ghidra.program.model.address.Address;
import ghidra.program.model.address.AddressRange;
import ghidra.program.model.address.AddressRangeImpl;
import ghidra.program.model.address.AddressSet;
import ghidra.program.model.lang.Register;
import ghidra.program.model.lang.RegisterValue;
import ghidra.program.model.listing.Instruction;
import ghidra.program.model.listing.InstructionIterator;
import ghidra.program.model.symbol.RefType;
import ghidra.program.model.symbol.Reference;
import ghidra.program.model.symbol.ReferenceIterator;

public class Thumb2ARM extends GhidraScript {
	@Override
	protected void run() throws Exception {
		ByteProvider provider = new MemoryByteProvider(currentProgram.getMemory(), currentProgram.getImageBase());
		BinaryReader reader = new BinaryReader(provider, false);

		AddressSet thumbSet = new AddressSet();
		AddressRange range = new AddressRangeImpl(
				currentProgram.getAddressFactory().getDefaultAddressSpace().getMinAddress(),
				currentProgram.getAddressFactory().getDefaultAddressSpace().getAddress(0x20000L));
		
		InstructionIterator it = currentProgram.getListing().getInstructions(new AddressSet(range), true);
		while (it.hasNext() && !monitor.isCancelled()) {
			Instruction ins = it.next();
			RegisterValue tmode = ins.getRegisterValue(ins.getRegister("TMode"));
			if (tmode.getUnsignedValue().equals(BigInteger.ONE)) {
				ReferenceIterator refIterTo = currentProgram.getReferenceManager().getReferencesTo(ins.getAddress());
				while (refIterTo.hasNext()) {
					Reference reference = refIterTo.next();
					if (reference.getReferenceType().equals(RefType.DATA) 
							&& reference.getFromAddress().getUnsignedOffset() < 0x20000L
							&& currentProgram.getListing().getInstructionAt(reference.getFromAddress()) == null) {
						Address dataAddr = reference.getFromAddress();
						reader.setPointerIndex(dataAddr.getUnsignedOffset());
						Long toAddr = reader.readNextUnsignedInt();
						if ((toAddr & 1) == 0) {
							println(String.format("tmode=1 for ref&1==0 @ 0x%x ; xref @ 0x%x => 0x%x",
										ins.getMinAddress().getUnsignedOffset(),
										dataAddr.getUnsignedOffset(),
										toAddr));
						}
						
						thumbSet.addRange(ins.getMinAddress(), ins.getMaxAddress());
					}
				}
			}
		}

		for (AddressRange thumbRange : thumbSet) {
			println(String.format("fix @ 0x%x .. 0x%x", thumbRange.getMinAddress().getUnsignedOffset(), thumbRange.getMaxAddress().getUnsignedOffset()));
			clearListing(thumbRange.getMinAddress(), thumbRange.getMaxAddress().add(1));
			new ArmDisassembleCommand(thumbRange.getMinAddress(), null, false).applyTo(currentProgram);
		}
		
		// Now fix leftover thumb mode instructions that are in the middle of ARM mode functions
		thumbSet = new AddressSet();
		it = currentProgram.getListing().getInstructions(new AddressSet(range), true);
		Instruction prevIns = null;
		while (it.hasNext() && !monitor.isCancelled()) {
			Instruction ins = it.next();
			if (prevIns != null) {
				RegisterValue prevTMode = prevIns.getRegisterValue(prevIns.getRegister("TMode"));
				RegisterValue tmode = ins.getRegisterValue(ins.getRegister("TMode"));
				if (prevTMode.getUnsignedValue().equals(BigInteger.ZERO) 
						&& tmode.getUnsignedValue().equals(BigInteger.ONE)
						&& !prevIns.getMnemonicString().equalsIgnoreCase("bx")
						&& !prevIns.getMnemonicString().equalsIgnoreCase("ldmia")
						&& !(prevIns.getMnemonicString().equalsIgnoreCase("ldr") && prevIns.getDefaultOperandRepresentation(0).equalsIgnoreCase("pc"))
						&& !(prevIns.getMnemonicString().equalsIgnoreCase("mov") && prevIns.getDefaultOperandRepresentation(0).equalsIgnoreCase("pc"))) {
					println(String.format("tmode=1 after tmode=0 @ 0x%x", ins.getMinAddress().getUnsignedOffset()));
					thumbSet.addRange(ins.getMinAddress(), ins.getMaxAddress());
				}
			}
			prevIns = ins;
		}

		for (AddressRange thumbRange : thumbSet) {
			println(String.format("fix @ 0x%x .. 0x%x", thumbRange.getMinAddress().getUnsignedOffset(), thumbRange.getMaxAddress().getUnsignedOffset()));
			clearListing(thumbRange.getMinAddress(), thumbRange.getMaxAddress().add(1));
			new ArmDisassembleCommand(thumbRange.getMinAddress(), null, false).applyTo(currentProgram);
		}
	}
}