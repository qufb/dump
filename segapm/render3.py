#!/usr/bin/env python3

from PIL import Image
import numpy as np
import sys


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


with open(sys.argv[1], "rb") as f:
    rom = f.read()

TILE_LEN = 1
PAL_ENTRIES = 0x100
PAL_OFFSET = 0x4882e
GFX_OFFSET = PAL_OFFSET + PAL_ENTRIES * 2
w = 24
h = 24 * 8

colors = rom[PAL_OFFSET + 2 : PAL_OFFSET + 2 + PAL_ENTRIES * 2]
rgb_colors = []
rgb_colors.append([255, 255, 255])
for i in range(0, 2 * PAL_ENTRIES, 2):
    color = colors[i : i + 2]
    color = int.from_bytes(color, byteorder="big")
    b = (color >> 10) & 0b11111
    g = (color >> 5) & 0b11111
    r = color & 0b11111
    rgb = [int(x * (255 / 31)) for x in [r, g, b]]
    rgb_colors.append(rgb)

idata = np.full((h, w, 3), 0xFF, dtype=np.uint8)
for y in range(h):
    for x in range(w):
        pal_i = rom[GFX_OFFSET + (y * w) + x]
        v = rgb_colors[pal_i]
        idata[y, x] = tuple(v)

img = Image.fromarray(idata, "RGB")
img.show()
img.save("out3.png")
