#!/usr/bin/env python3

from PIL import Image
from pathlib import Path
import struct
import numpy as np
import sys


def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


def rgb555_to_rgb888(col):
    r = (col >> 10) & 0x1F
    g = (col >> 5) & 0x1F
    b = (col >> 0) & 0x1F
    r = (r << 3) | (r >> 2)
    g = (g << 3) | (g >> 2)
    b = (b << 3) | (b >> 2)
    return (r, g, b)


def parse_pal(rgb555s):
    pal = []
    for i in range(0, 0x200, 2):
        rgb555 = struct.unpack(">H", rgb555s[i : i + 2])[0]
        pal.append(rgb555_to_rgb888(rgb555))
    return pal


pal_size = 0x200
pal_full = 0x800
w = 0x50
h = 0x66
frame_size = w * h
frame_full = 0x2000
anim_chunk = frame_full * 30 + pal_full

ani_name = sys.argv[1]
with open(ani_name, "rb") as f:
    ani = f.read()

out_dir = Path(f"out_{ani_name}")
out_dir.mkdir(parents=True, exist_ok=True)

TILE_LEN = 2
p = 0
num_chunks = len(ani) // anim_chunk
for i in range(num_chunks):
    imgs = []
    pal = parse_pal(ani[p : p + 0x200])
    p += pal_full
    frame_name = "{:04d}_{:08X}".format(i, p)
    print(frame_name)
    for j in range(30):
        frame = ani[p : p + frame_size]
        img_data = []
        for pal_i in frame:
            rgb = pal[pal_i]
            bgr = (rgb[2], rgb[1], rgb[0])
            img_data.append(bgr)

        idata = np.full(((h + 1) * TILE_LEN, w * TILE_LEN, 3), 0xFF, dtype=np.uint8)
        for ti in range(len(img_data)):
            wi = ti % w
            hi = ti // w
            for i in range(0, TILE_LEN):
                for j in range(0, TILE_LEN):
                    v = img_data[ti]
                    idata[(hi * TILE_LEN) + j, (wi * TILE_LEN) + i] = tuple(v)

        imgs.append(Image.fromarray(idata, "RGB").quantize(method=Image.MEDIANCUT))
        # imgs.append(Image.fromarray(idata, "RGB"))
        # img = Image.fromarray(idata, "RGB")
        # img.save(Path(out_dir / f'{frame_name}.png').absolute())

        p += frame_full

    imgs[0].save(
        Path(out_dir / f"{frame_name}.gif").absolute(),
        save_all=True,
        append_images=imgs[1:],
        duration=67,
        loop=0,
    )
