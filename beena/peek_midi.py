#!/usr/bin/env python3

import gdb


def peek(addr):
    gdb.execute(f"x/10wx {hex(addr)}")


base = 0x70000000
step = 0x10
for x in range(2):
    for i in range(step):
        for j in range(step):
            addr = base + (x * 0x1000000) + (i * 0x100000) + (j * 0x10000)
            peek(addr)
            addr += 0x7FF0
            peek(addr)
            addr += 0x8000
            peek(addr)
