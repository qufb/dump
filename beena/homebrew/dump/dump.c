#include "stdio.h"

#define DUMP_OFFSET 0x200
#define DUMP_LENGTH 0x100

__attribute__((section (".text:set_work_print_tiles")))
void set_work_print_tiles() {}

__attribute__((section (".text:print")))
void print() {}

__attribute__((section (".text:writes_to_40000010_toggle_1")))
void writes_to_40000010_toggle_1(int i) {}

__attribute__((section (".text:writes_to_40000010_toggle_2")))
void writes_to_40000010_toggle_2(int i) {}

__attribute__((section (".text:init_work_print_buf")))
void init_work_print_buf(int i, int x, int y) {}

__attribute__((section (".text:init_io")))
void init_io(void *main_oggs) {}

__attribute__((section (".text:writes_work_default_pal")))
void writes_work_default_pal(unsigned short pal, unsigned int i) {}

__attribute__((section (".text:do_callbacks")))
void do_callbacks() {}

__attribute__((section (".text:hexdump")))
void hexdump() {
    set_work_print_tiles();
    writes_to_40000010_toggle_2(1);
    writes_work_default_pal(0x4210, 0);
    do_callbacks();
    writes_to_40000010_toggle_1(1);
    init_work_print_buf(0,3,3);
    print("READ %08X..%08X\n", (unsigned long)(DUMP_OFFSET + 0), (unsigned long)(DUMP_OFFSET + DUMP_LENGTH));
    for (int i = 0x0; i < DUMP_LENGTH; i += 0x10) {
        for (int j = 0; j < 0x10; j++) {
            print("%02X", *(unsigned char*)(DUMP_OFFSET + i + j));
        }
        print("\n");
    }
    for (;;) {
        do_callbacks();
    }
}

__attribute__((section (".text:main")))
int main() {
    init_io(NULL);
    hexdump();
    __asm__("mov r7, r7"); // Pad 2 bytes to avoid bad instruction at 0x80000f5a, maybe unneeded...
    return 0;
}
