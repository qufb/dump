#!/usr/bin/env python3

from PIL import Image
import numpy as np
import sys


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


TILE_LEN = 8

input_name = sys.argv[1]
with open(input_name, "rb") as f:
    colors = f.read()

rgb_pixels = []
for i in range(0, len(colors), 4):
    color = colors[i : i + 2]
    color = int.from_bytes(color, byteorder="big")
    b = (color >> 10) & 0b11111
    g = (color >> 5) & 0b11111
    r = color & 0b11111
    rgb = [int(x * (255 / 31)) for x in [r, g, b]]
    rgb_pixels.append(rgb)

w = 704
h = 480
idata = np.zeros(
    (h, w, 3), dtype=np.uint8
)
tdata = rgb_pixels
for j in range(len(rgb_colors)):
    v = tdata[j]
    for i in range(0, w):
        idata[j, i] = tuple(v)
img = Image.fromarray(idata, "RGB")
img.show()
img.save("out3.pal.png")
