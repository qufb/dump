#include "stdio.h"

#define DUMP_OFFSET 0x200
#define DUMP_LENGTH 0x100

__attribute__((naked, section (".text:ext_22c_store")))
void ext_22c_store(unsigned int addr, unsigned int v) { __asm__("bx lr"); }

__attribute__((section (".text:lib_strncpy")))
void lib_strncpy(unsigned int dst, unsigned int src, int n) {}

__attribute__((section (".text:lib_memset")))
void lib_memset(unsigned int dst, char v, int n) {}

__attribute__((section (".text:write_edinburgh")))
void write_edinburgh() {}

__attribute__((section (".text:init_vars")))
void init_vars() {}

__attribute__((section (".text:cb_init")))
void cb_init() {}

__attribute__((section (".text:init_audio")))
void init_audio() {}

__attribute__((section (".text:set_work_print_tiles")))
void set_work_print_tiles() {}

__attribute__((section (".text:print")))
void print() {}

__attribute__((section (".text:writes_to_40000010_toggle_1")))
void writes_to_40000010_toggle_1(int i) {}

__attribute__((section (".text:writes_to_40000010_toggle_2")))
void writes_to_40000010_toggle_2(int i) {}

__attribute__((section (".text:init_work_print_buf")))
void init_work_print_buf(int i, int x, int y) {}

__attribute__((section (".text:init_io")))
void init_io(void *main_oggs) {}

__attribute__((section (".text:writes_work_default_pal")))
void writes_work_default_pal(unsigned short pal, unsigned int i) {}

__attribute__((section (".text:do_callbacks")))
void do_callbacks() {}

__attribute__((section (".text:hexdump")))
void hexdump() {
    set_work_print_tiles();
    writes_to_40000010_toggle_2(1);
    writes_work_default_pal(0x4210, 0);
    do_callbacks();
    writes_to_40000010_toggle_1(1);
    init_work_print_buf(0, 3, 3);
    print("READ %08X..%08X\n", (unsigned long)(DUMP_OFFSET + 0), (unsigned long)(DUMP_OFFSET + DUMP_LENGTH));
    for (int i = 0x0; i < DUMP_LENGTH; i += 0x10) {
        for (int j = 0; j < 0x10; j++) {
            print("%02X", *(unsigned char*)(DUMP_OFFSET + i + j));
        }
        print("\n");
    }
    for (;;) {
        do_callbacks();
    }
}

__attribute__((naked, section (".text:main")))
int main() {
    ext_22c_store(0x60020000, 0x12009000);
    ext_22c_store(0x60020004, 0x1e9);
    lib_strncpy(0xc00cc000, 0x8075291c, 0x000031e8);
    lib_memset(0xc00cf1e8, 0, 0x7bf8);
    write_edinburgh();
    init_vars();
    cb_init();
    init_audio();

    init_io(NULL);
    hexdump();

    for (;;) {}

    return 0;
}
