// license:BSD-3-Clause
// copyright-holders:QUFB

#include "emu.h"

#include "foo.h"

DEFINE_DEVICE_TYPE(FOO, foo_device, "foo", "Foo")

static INPUT_PORTS_START( foo )
INPUT_PORTS_END

ioport_constructor foo_device::device_input_ports() const
{
	return INPUT_PORTS_NAME( foo );
}

foo_device::foo_device(const machine_config &mconfig, const char *tag, device_t *owner, uint32_t clock)
    : device_t(mconfig, FOO, tag, owner, clock)
{ }

void foo_device::device_start()
{
}

void foo_device::device_reset()
{
}

void foo_device::device_add_mconfig(machine_config &config)
{
}

