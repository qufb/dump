.set noreorder

.global __start
.text
__start:
# 8004af2c 00 fc 07 08     j          LAB_801ff000
# 8004af30 00 00 00 00     _nop
j cave
nop

.section ".text:cave"
cave:
# get VLO index
lui $at,0x800c
addu $at,$at,$s0
lw $s1,-0x3ac0($at)
# is VLO for level 19?
ori $a0, $0, 0x16f
bne $a0, $s1, cave_end
nop

cave_extra:
# load VLO for DragonDan
ori $a0, $0, 0x220
jal read_res
nop
ori $a0, $0, 0x220
jal read_res_after
nop
ori $a0, $0, 0x220
jal load_vlo_after
nop

cave_end:
# Execute original instructions patched by trampoline
lui $s1,0x800d
addiu $s1,$s1,0x79d8
# Back to original function
j 0x8004af34
nop
nop

.section ".text:read_res"
read_res:
jr $ra
nop
.section ".text:read_res_after"
read_res_after:
jr $ra
nop
.section ".text:load_vlo_after"
load_vlo_after:
jr $ra
nop
