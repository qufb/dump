#!/usr/bin/env python3

"""
Dumps all textures contained in "2GRV" sections.

Usage: ./rip.py medres.mwd

Tested with the following "medres.mwd" versions:
- Rolling Demo (sha1: e34c18576cae97cc1b4b8559aa0aaaa908cb460d)
- Retail SCUS-94227 (sha1: 3fec06899ef524f53da80fe0f385b8dcd479eb8e)

Adapted from: https://web.archive.org/web/20150614120034/http://sirdanielfortesque.proboards.com/thread/965/medievil-rips

TODO: Apply transparency
"""

import struct
import sys

from PIL import Image


def from16(p):
    return ((p & 0x1F) << 3, (p >> 2) & 0xF8, (p >> 7) & 0xF8, p >> 15)


with open(sys.argv[1], "rb") as f:
    d = f.read()

i = -1
while True:
    clut_map = {}

    i = d.find(b"2GRV", i + 1)
    if i == -1:
        break

    si = i
    print("graphics chunk @ %#x" % si)

    i += 4
    num_img, off_img, num_stuff, off_stuff = struct.unpack("<4I", d[i : i + 4 * 4])

    print("\t%d images @ offset %#x" % (num_img, off_img))

    i += 4 * 4

    (
        unk_x,
        unk_y,
        width,
        height,
        offset,
        whatever1,
        clut,
        texpage,
        whatever2,
    ) = struct.unpack("<2H2HII2HI", d[i + (num_img - 1) * 24 : i + num_img * 24])
    pal_off = si + offset + width * 2 * height

    # FIXME: Retail US version hack... Why is there an off-by-one (1 pixel = 2 bytes)? Are palettes 4-bytes aligned?
    print("\tsi + off = %#x" % (si + offset))
    if si + offset == 0x1B284:
        pal_off += 2
    # jp
    elif si + offset == 0x1B99C:
        pal_off += 2
    # timed demo
    elif si + offset == 0x1BA18:
        pal_off += 2
    # 0.28
    elif si + offset == 0x1B328:
        pal_off += 2

    print("\tpal off @ %#x" % pal_off)

    next_pal_off = pal_off
    for n in range(num_img):
        prev_clut = clut
        (
            unk_x,
            unk_y,
            width,
            height,
            offset,
            whatever1,
            texpage,
            clut,
            whatever2,
            whatever3,
        ) = struct.unpack("<2H2HIHHHHI", d[i : i + 24])
        mode = (texpage >> 7) & 3

        if mode == 0:
            assert clut
            width *= 4
        elif mode == 1:
            assert clut
            width *= 2
        elif mode == 2:
            assert not clut
        else:
            assert False, "weird mode"

        print(
            "\t\timage @ offset %#x (rel: %#x): %dx%dpx, mode %d, VRAM pos? (%d, %d), CLUT? %#x, texpage? %#x, extra %08x:%08x:%08x"
            % (
                si + offset,
                offset,
                width,
                height,
                mode,
                unk_x,
                unk_y,
                clut,
                texpage,
                whatever1,
                whatever2,
                whatever3,
            )
        )
        soffset = si + offset

        if clut in clut_map:
            print(f"\t\t!!! clut cached @ {hex(soffset)} -> {hex(clut)}")
            pal_off = clut_map[clut]
        else:
            # Skip modes without valid clut values
            if mode != 2:
                clut_map[clut] = pal_off
            next_pal_off = pal_off
            next_mode = mode

        im = Image.new("RGBA", (width, height))
        imd = im.load()
        pn = 0
        for y in range(height):
            for x in range(width):
                if mode == 0:
                    pi = pal_off + ((d[soffset + (pn >> 1)] >> ((x & 1) * 4)) & 0xF) * 2
                elif mode == 1:
                    pi = pal_off + d[soffset + pn] * 2
                elif mode == 2:
                    pi = soffset + (pn << 1)

                r, g, b, a = from16(struct.unpack("<H", d[pi : pi + 2])[0])
                imd[x, y] = (r, g, b)

                pn += 1

        im.save(
            f"out/img_@{si + offset:08}_@{si + offset:06x}_({si:06x}_{offset:06x}).png"
        )

        i += 24

        (
            next_unk_x,
            next_unk_y,
            next_width,
            next_height,
            next_offset,
            next_whatever1,
            next_texpage,
            next_clut,
            next_whatever2,
            next_whatever3,
        ) = struct.unpack("<2H2HIHHHHI", d[i : i + 24])

        # Palette sizes taken from: https://psx-spx.consoledev.net/graphicsprocessingunitgpu/
        pal_off = next_pal_off
        if next_clut != clut:
            if next_mode == 0:
                pal_off += 16 * 2
            elif next_mode == 1:
                pal_off += 256 * 2
        print(f"\t\t--- pal_off: {hex(pal_off)}")
