#!/usr/bin/awk -f

BEGIN { p=0; }

/^TRACE_BEGIN/ { p=1; print; next; }
/^TRACE_END/ { p=0; print; next; }

{ if (p==1) print; fflush(); }
