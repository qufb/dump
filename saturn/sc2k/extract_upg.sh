#!/bin/sh

set -eu

state=$1 # Taken with Kronos
cdrom=$2 # Path to track1.bin dir mounted with fuseiso

for y in 1950 2050; do
  rm -rf ./chunks
  mkdir -p ./chunks

  ./clear_dump.py "$state"
  ./prepare_dump.py "$cdrom"/tiles/y"$y"upg.hed "$cdrom"/tiles/y"$y"upg.dat "$state"
  mame -window saturn_test -skip_gameinfo -switchres -nofilter -nounevenstretch -verbose -cart "$state" || true
  ./tile.py "$cdrom"/sc2000.dat "$state"

  mkdir -p ./out_cdrom/tiles
  mv ./out ./out_cdrom/tiles/y"$y"upg.dat
done
