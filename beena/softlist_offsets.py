#!/usr/bin/env python3

import re
import sys

from bs4 import BeautifulSoup
from bs4.formatter import XMLFormatter


class UnsortedAttributes(XMLFormatter):
    def attributes(self, tag):
        for k, v in tag.attrs.items():
            yield k, v


if __name__ == "__main__":
    filename = sys.argv[1]
    with open(filename) as data:
        soup = BeautifulSoup(
            data.read(),
            "xml",
            preserve_whitespace_tags=["description", "year", "publisher"],
        )

    tag = "dataarea"
    dataareas = soup.findAll(tag)
    for dataarea in dataareas:
        name = dataarea.get("name")
        #if name == "rom":
        #    del dataarea["width"]
        #    del dataarea["endianness"]
        if name == "pages":
            i = 0
            step = 0x02500000
            roms = dataarea.findChildren("rom")
            for rom in roms:
                rom["offset"] = f"{step * i:#010x}"
                i += 1
            dataarea["size"] = f"{step * i:#010x}"

    with open("out", "w") as f:
        formatter = UnsortedAttributes(indent=4)
        f.write(soup.prettify(formatter=formatter))

    out = ""
    with open("out", "r") as f:
        for line in f.readlines():
            if re.match("^[ \t]*<!-- book pages -->[ \t]*$", line):
                line = re.sub('<!-- book pages -->', '<!-- booklet pages, max 0x02500000 bytes per page -->', line)
            elif re.match(re.escape('<?xml version="1.0" encoding="utf-8"?>'), line):
                line = '<?xml version="1.0"?>\n'
            elif re.match('^[ \t]*' + re.escape('<!-- you must byteswap ROMs'), line):
                line = line + '\n'
            elif re.match('^[ \t]*' + re.escape('</software>'), line):
                line = line + '\n'
            for i in range(20, 1, -1):
                line = re.sub("^" + (4 * i * " "), i * "\t", line)
            line = re.sub("^    ", "\t", line)
            line = re.sub(" & ", " &amp; ", line)
            out += line

    with open("out", "w") as f:
        f.write(out)
