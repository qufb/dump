#!/usr/bin/env python3

import collections
import hashlib
import pathlib
import random
import re
import string
import sys
import time
import zlib

from bs4 import BeautifulSoup
from bs4.formatter import XMLFormatter

from requests_html import HTML, HTMLSession


class UnsortedAttributes(XMLFormatter):
    def attributes(self, tag):
        for k, v in tag.attrs.items():
            yield k, v


def generate_software_entry(soup, romdirs):
    name_jp = "テレビとお茶札 お茶犬『ほっ』と生活"
    name_en = "TV to Ocha-Satsu Ocha-Ken Hotsu to Seikatsu"
    publisher = "Sega Toys"
    datePublished = "2005"
    print(name_en)

    entries = []
    for target_rom_dir in romdirs:
        rom_file = next(target_rom_dir.glob("*.u3"))
        rom_data = open(rom_file, "rb").read()
        crc = zlib.crc32(rom_data) & 0xFFFFFFFF
        sha = hashlib.sha1(rom_data).hexdigest().lower()

        software_name = "tvochken"
        software_attrs = {"name": software_name, "supported": "no"}
        software = soup.new_tag("software", attrs=software_attrs)
        desc = soup.new_tag("description")
        desc.string = name_en
        year = soup.new_tag("year")
        year.string = datePublished
        publ = soup.new_tag("publisher")
        publ.string = publisher
        info = soup.new_tag("info", attrs={"name": "alt_title", "value": name_jp})
        software.append(desc)
        software.append(year)
        software.append(publ)
        software.append(info)

        part = soup.new_tag(
            "part", attrs={"name": "cart", "interface": "sega_beena_cart"}
        )
        dataarea = soup.new_tag(
            "dataarea",
            attrs={
                "name": "rom",
                "size": hex(len(rom_data)),
                "width": "32",
                "endianness": "big",
            },
        )
        rom = soup.new_tag(
            "rom",
            attrs={
                "loadflag": "load16_word_swap",
                "name": rom_file.name,
                "size": hex(len(rom_data)),
                "crc": f"{crc:08x}",
                "sha1": sha,
            },
        )
        dataarea.append(rom)
        part.append(dataarea)

        deck_dir = target_rom_dir
        if (target_rom_dir / "Cards").exists():
            deck_dir = target_rom_dir / "Cards"
        for i, card_file in enumerate(sorted(deck_dir.glob("*.png"))):
            rom_data = open(card_file, "rb").read()
            size = f"0x{len(rom_data):08x}"
            crc = zlib.crc32(rom_data) & 0xFFFFFFFF
            sha = hashlib.sha1(rom_data).hexdigest().lower()
            dataarea = soup.new_tag(
                "dataarea",
                attrs={
                    "name": f"card{i + 1}",
                    "size": size,
                },
            )
            rom = soup.new_tag(
                "rom",
                attrs={
                    "name": card_file.name,
                    "size": f"0x{len(rom_data):08x}",
                    "crc": f"{crc:08x}",
                    "sha1": sha,
                },
            )
            dataarea.append(rom)
            part.append(dataarea)

        software.append(part)

        entries.append({"name": software_name, "data": software})

    return entries


if __name__ == "__main__":
    rompath = sys.argv[1]
    romdirs = [pathlib.Path(rompath)]

    soup = BeautifulSoup(
        features="xml", preserve_whitespace_tags=["description", "year", "publisher"]
    )

    softwarelist = soup.new_tag(
        "softwarelist",
        attrs={"name": "sega_beena_cart", "description": "Sega Beena cartridges"},
    )
    software_entries = {}
    for entry in generate_software_entry(soup, romdirs):
        software_entries[entry["name"]] = entry["data"]
    for _, data in collections.OrderedDict(sorted(software_entries.items())).items():
        softwarelist.append(data)

    soup.append(softwarelist)

    out_filename = "out.xml"
    formatter = UnsortedAttributes(indent=4)
    with open(out_filename, "w") as f:
        f.write(soup.prettify(formatter=formatter))

    out = ""
    with open(out_filename, "r") as f:
        for line in f.readlines():
            if re.match(re.escape('<?xml version="1.0" encoding="utf-8"?>'), line):
                line = '<?xml version="1.0"?>\n'
                line += '<!DOCTYPE softwarelist SYSTEM "softwarelist.dtd">\n'
                line += "<!--\n"
                line += "license:CC0-1.0\n"
                line += "-->\n"
            elif re.match(re.escape("<softwarelist"), line):
                line = line + "\n"
            elif re.match("^[ \t]*" + re.escape("</software>"), line):
                line = line + "\n"
            for i in range(20, 1, -1):
                line = re.sub("^" + (4 * i * " "), i * "\t", line)
            line = re.sub("^    ", "\t", line)
            line = re.sub(" & ", " &amp; ", line)
            out += line

    with open(out_filename, "w") as f:
        f.write(out)
