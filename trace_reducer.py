#!/usr/bin/env python3

import argparse
import signal
import time
import os

addresses = {}


def handler(signum, frame):
    out = f"trace.{int(time.time())}.out"
    output(out)
    print(f"Snapshot written to {out}.")


def output(out, count=False):
    if os.path.exists(out):
        os.remove(out)
    with open(out, "w") as f:
        for a in sorted(map(lambda x: x.replace(':', '').strip().zfill(8), addresses.keys())):
            if count:
                f.write(f"{addresses[a]:>8} {a}")
            else:
                f.write(a)
            f.write("\n")


if __name__ == "__main__":
    signal.signal(signal.SIGINT, handler)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o",
        "--out",
        type=str,
        default="trace.out",
        help="Output file path",
    )
    parser.add_argument(
        "-s",
        "--size",
        type=int,
        default=8,
        help="Address size to match at beginning of line",
    )
    parser.add_argument(
        "-c",
        "--count",
        default=False,
        action="store_true",
        help="Include count of hits per instruction",
    )
    parsed_args = parser.parse_args()

    fifo = "f"
    if os.path.exists(fifo):
        os.remove(fifo)
    os.mkfifo(fifo)

    with open(fifo) as f:
        for line in f:
            address = line[: parsed_args.size]
            if address not in addresses:
                addresses[address] = 0
            addresses[address] += 1

    output(parsed_args.out, parsed_args.count)
