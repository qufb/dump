#!/usr/bin/env python3

import collections
import hashlib
import pathlib
import random
import re
import string
import sys
import time
import zlib

from bs4 import BeautifulSoup
from bs4.formatter import XMLFormatter


class UnsortedAttributes(XMLFormatter):
    def attributes(self, tag):
        for k, v in tag.attrs.items():
            yield k, v


def generate_software_entry(soup, romdirs):
    for target_rom_dir in romdirs.glob("*"):
        print(target_rom_dir)
        rom_file = next(target_rom_dir.glob("*.bin"))
        rom_data = open(rom_file, "rb").read()
        crc = zlib.crc32(rom_data) & 0xFFFFFFFF
        sha = hashlib.sha1(rom_data).hexdigest().lower()

        softwares = soup.find_all("software")
        for software in softwares:
            desc = software.find("description")
            if desc.text.strip() not in str(rom_file):
                continue

            pages_dir = target_rom_dir
            if (target_rom_dir / "Booklet").exists():
                pages_dir = target_rom_dir / "Booklet"
            part = software.find("part", attrs={"name": "cart", "interface": "copera_cart"})
            for i, card_file in enumerate(sorted(pages_dir.glob("*.png"))):
                rom_data = open(card_file, "rb").read()
                size = f"0x{len(rom_data):07x}"
                crc = zlib.crc32(rom_data) & 0xFFFFFFFF
                sha = hashlib.sha1(rom_data).hexdigest().lower()
                dataarea = soup.new_tag(
                    "dataarea",
                    attrs={
                        "name": f"page{i + 1}",
                        "size": size,
                    },
                )
                rom = soup.new_tag(
                    "rom",
                    attrs={
                        "name": f"Booklet/{card_file.name}",
                        "size": f"0x{len(rom_data):07x}",
                        "crc": f"{crc:08x}",
                        "sha1": sha,
                    },
                )
                dataarea.append(rom)
                part.append(dataarea)


if __name__ == "__main__":
    base_xml = sys.argv[1]
    rompath = sys.argv[2]
    romdirs = pathlib.Path(rompath)

    with open(base_xml, "rb") as f:
        soup = BeautifulSoup(f.read(), features="xml", preserve_whitespace_tags=["description", "year", "publisher"]
    )

    generate_software_entry(soup, romdirs)

    out_filename = "out.xml"
    formatter = UnsortedAttributes(indent=4)
    with open(out_filename, "w") as f:
        f.write(soup.prettify(formatter=formatter))

    out = ""
    with open(out_filename, "r") as f:
        for line in f.readlines():
            if re.match(re.escape('<?xml version="1.0" encoding="utf-8"?>'), line):
                line = '<?xml version="1.0"?>\n'
            elif re.match("^" + re.escape('-->'), line):
                line = "-->\n\n"
            elif re.match(re.escape("<softwarelist"), line):
                line = line + "\n\n"
            elif re.match("^[ \t]*" + re.escape("</software>"), line):
                line = line + "\n"
            elif re.match("^[ \t]*" + re.escape("</softwarelist>"), line):
                line = line + "\n"
            for i in range(20, 1, -1):
                line = re.sub("^" + (4 * i * " "), i * "\t", line)
            line = re.sub("^    ", "\t", line)
            out += line

    with open(out_filename, "w") as f:
        f.write(out)
