#!/usr/bin/env python3

from PIL import Image
import math
import numpy as np
import sys


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


input_name = sys.argv[1]
w = int(sys.argv[2], 0)

with open(input_name, "rb") as f:
    input_bytes = f.read()

TILE_LEN = 8
fonts = [[0x748F14, 0x746892]]

rgb_pixels = []
for font in fonts:
    pal_offset = font[0]
    tiles_offset = font[1]
    rgb_colors = []
    num_colors = int.from_bytes(
        input_bytes[pal_offset : pal_offset + 2], byteorder="big"
    )
    colors = input_bytes[pal_offset + 2 : pal_offset + 2 + num_colors * 2]
    print(num_colors, colors)

    color = int.from_bytes(b"\x42\x10", byteorder="big")
    b = (color >> 10) & 0b11111
    g = (color >> 5) & 0b11111
    r = color & 0b11111
    rgb = [int(x * (255 / 31)) for x in [r, g, b]]
    rgb_x = rgb

    rgb_colors.append([255, 255, 255])
    for i in range(0, 2 * num_colors, 2):
        color = colors[i : i + 2]
        color = int.from_bytes(color, byteorder="big")
        b = (color >> 10) & 0b11111
        g = (color >> 5) & 0b11111
        r = color & 0b11111
        rgb = [int(x * (255 / 31)) for x in [r, g, b]]
        rgb_colors.append(rgb)
    print(rgb_colors)

    # tile_size = int.from_bytes(
    #    input_bytes[tiles_offset : tiles_offset + 0x1], byteorder="big"
    # )
    tile_size = TILE_LEN * TILE_LEN
    tiles_count = int.from_bytes(
        input_bytes[tiles_offset + 0x1 : tiles_offset + 0x3], byteorder="big"
    )
    print(hex(tile_size), hex(tiles_count))
    tiles = input_bytes[
        tiles_offset - 0x4 + 0x4 : tiles_offset - 0x4 + 0x4 + tile_size * tiles_count
    ]
    for pal_i in tiles:
        if pal_i > len(rgb_colors) - 1:
            rgb_pixels.append(rgb_x)
        else:
            rgb_pixels.append(rgb_colors[pal_i])

rgb_tiles = list(chunks(rgb_pixels, TILE_LEN * TILE_LEN))
print(len(rgb_tiles))
# print(rgb_tiles)

idata = np.full(
    (((len(rgb_tiles) // w) + 1) * TILE_LEN, w * TILE_LEN, 3), 0xFF, dtype=np.uint8
)
tdata = rgb_tiles
for ti in range(len(rgb_tiles)):
    wi = ti % w
    hi = ti // w
    for i in range(0, TILE_LEN):
        for j in range(0, TILE_LEN):
            v = tdata[ti][i + (j * TILE_LEN)]
            idata[(hi * TILE_LEN) + j, (wi * TILE_LEN) + i] = tuple(v)
img = Image.fromarray(idata, "RGB")
img.show()
img.save(f"out2.font.png")
