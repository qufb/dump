#include <sys/mman.h>
#include "stdio.h"

int main() {
    //int *var = (int*)0x200;
    int *var = (int*)mmap( 0x200, 0x100, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0 );
    *var = 0xbe;
    *(var+1) = 0xee;

    //for (int i = 0x200; i < 0x260; i += 0x10) {
    printf("READ %08X..%08X\n", (unsigned long)(var + 0), (unsigned long)(var + 0x60));
    for (int i = 0x0; i < 0x60; i += 0x10) {
        for (int j = 0; j < 0x10; j++) {
            printf("%02X", *(unsigned char*)(var + i + j));
        }
        printf("\n");
    }
    return 0;
}
