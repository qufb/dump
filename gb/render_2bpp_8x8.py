#!/usr/bin/env python3

from PIL import Image
import math
import numpy as np
import pprint
import struct
import sys

TILE_LEN = 8
TILE_BPP = 2
A_PAL = [
    [0x00, 0x00, 0x00],
    [0x40, 0x40, 0x40],
    [0x80, 0x80, 0x80],
    [0xff, 0xff, 0xff],
]
A_PAL_GB = [
    [0x00, 0x00, 0x00],
    [0x10, 0x09, 0x10],
    [0x1f, 0x10, 0x00],
    [0x1f, 0x1b, 0x00],
]
A_PAL=[]
for p in A_PAL_GB:
    A_PAL.append([
        p[0] * 0xff // 0x1f,
        p[1] * 0xff // 0x1f,
        p[2] * 0xff // 0x1f,
    ])
A_PAL=[
    [0x00, 0x00, 0x00],
    [0xb4, 0x92, 0xb4],
    [0xf6, 0xa2, 0x48],
    [0xf9, 0xcf, 0x52],
]

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]

def tfill(idata, tdata, pal, wi, hi, ti):
    for i in range(0, TILE_LEN):
        for j in range(0, TILE_LEN):
            v = tdata[(ti * TILE_LEN * TILE_LEN) + (i + (j * TILE_LEN))]
            idata[(hi * TILE_LEN) + j, (wi * TILE_LEN) + i] = tuple(pal[v])
    return idata


def parse_pal(input_bytes, p):
    pal = []
    for i in range(16):
        pal_bytes = int(struct.unpack(">h", input_bytes[p : p + 2])[0])
        p += 2

        r = (pal_bytes & 0xF) << 4
        g = ((pal_bytes >> 4) & 0xF) << 4
        b = ((pal_bytes >> 8) & 0xF) << 4
        pal.append([r, g, b])
        pprint.pprint(pal)

    return pal


def render_cmap(input_bytes, pal, p, output_name):
    w = int(struct.unpack(">h", input_bytes[p : p + 2])[0])
    p += 2
    h = int(struct.unpack(">h", input_bytes[p : p + 2])[0])
    p += 2
    tile_count = int(struct.unpack(">h", input_bytes[p : p + 2])[0])
    p += 2
    print(w,h,tile_count)

    iw = w * TILE_LEN
    ih = h * TILE_LEN
    idata = np.zeros((ih, iw, 3), dtype=np.uint8)
    tdata = [0] * TILE_LEN * TILE_LEN * w * h
    tile_i = p

    tile_i_len = 2
    pal_i = tile_i + (w * h * tile_i_len)
    for i in range(0, tile_count * TILE_LEN * TILE_BPP):
        v_i = pal_i + i
        pal_bytes = input_bytes[v_i : v_i + 1]
        v1 = (int(struct.unpack(">b", pal_bytes)[0]) & 0xF0) >> 4
        v2 = int(struct.unpack(">b", pal_bytes)[0]) & 0xF
        tdata[i * 2] = v1
        tdata[i * 2 + 1] = v2

    for j in range(0, h):
        for i in range(0, w):
            v = int(struct.unpack(">h", input_bytes[tile_i : tile_i + 2])[0])
            if v > tile_count:
                print(f"FIXME: @{hex(tile_i)}: {hex(v)}")
                v = v & 0x1FF
            idata = tfill(idata, tdata, pal, i, j, v)
            tile_i += 2

    img = Image.fromarray(idata, "RGB")
    # img.show()
    img.save(f"{output_name}.png")


def render_tiles(input_bytes, pal, p, w, h, output_name):
    tile_count = w * h

    iw = w * TILE_LEN
    ih = h * TILE_LEN
    idata = np.zeros((ih, iw, 3), dtype=np.uint8)
    tdata = [0] * TILE_LEN * TILE_LEN * w * h
    tile_i = p

    tile_i_len = 2
    pal_i = tile_i + (w * h * tile_i_len)
    for i in range(0, tile_count * TILE_LEN * TILE_BPP):
        v_i = pal_i + i
        pal_bytes = input_bytes[v_i : v_i + 1]
        v1 = (int(struct.unpack(">b", pal_bytes)[0]) & 0xF0) >> 4
        v2 = int(struct.unpack(">b", pal_bytes)[0]) & 0xF
        tdata[i * 2] = v1
        tdata[i * 2 + 1] = v2

    v = 0
    for j in range(0, h):
        for i in range(0, w):
            idata = tfill(idata, tdata, pal, i, j, v)
            v += 1

    img = Image.fromarray(idata, "RGB")
    img.show()
    img.save(f"{output_name}.png")



def to_png(input_name, output_name="out"):
    with open(input_name, "rb") as f:
        input_bytes = f.read()

    decompressed_len = int(struct.unpack(">L", input_bytes[:4])[0])
    num_elements = int(struct.unpack(">h", input_bytes[4:6])[0])
    p = 6
    elements = {}  # [name, offset]
    for i in range(num_elements):
        elements[i] = [input_bytes[p : p + 4], 0]
        p += 4
    for i in range(num_elements):
        elements[i][1] = struct.unpack(">L", input_bytes[p : p + 4])[0]
        p += 4

    picked_pal = A_PAL
    for i in range(num_elements):
        name = str(elements[i][0],'ascii')
        if name == "!pal":
            picked_pal = parse_pal(input_bytes, p + elements[i][1])
            break

    for i in range(num_elements):
        name = str(elements[i][0],'ascii')
        if name != "!pal":
            render_cmap(input_bytes, picked_pal, p + elements[i][1], output_name)


if __name__ == "__main__":
    input_name = sys.argv[1]

    start_offset = int(sys.argv[2], 0)
    tile_count = int(sys.argv[3], 0)
    w = int(sys.argv[4], 0)
    with open(input_name, "rb") as f:
        input_bytes = f.read()[start_offset:start_offset + (2 * TILE_LEN * tile_count)]

    #render_tiles(input_bytes, A_PAL, 0, w, h, "out")
    pal_indexes=[]
    for i in range(0, len(input_bytes), 2):
        msbits=input_bytes[i+1]
        lsbits=input_bytes[i]
        for j in range(8):
            mask = 7 - j
            pal_index = (((msbits >> mask) & 1) << 1) + ((lsbits >> mask) & 1)
            pal_indexes.append(pal_index)

    rgb_pixels=[]
    #for i8 in chunks(pal_indexes, 8):
    for i in pal_indexes:
        rgb_pixels.append(A_PAL[i])
    rgb_tiles=list(chunks(rgb_pixels, 8*8))
    print(rgb_tiles)

    idata = np.zeros((((len(rgb_tiles)//w)+1)*TILE_LEN, w*TILE_LEN, 3), dtype=np.uint8)
    tdata=rgb_tiles
    for ti in range(len(rgb_tiles)):
        wi = ti % w
        hi = ti // w
        for i in range(0, TILE_LEN):
            for j in range(0, TILE_LEN):
                v = tdata[ti][i + (j * TILE_LEN)]
                idata[(hi * TILE_LEN) + j, (wi * TILE_LEN) + i] = tuple(v)
    img = Image.fromarray(idata, "RGB")
    img.save(f"out.png")
