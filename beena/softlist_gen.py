#!/usr/bin/env python3

import collections
import hashlib
import pathlib
import random
import re
import string
import sys
import time
import unicodedata
import zlib

from bs4 import BeautifulSoup
from bs4.formatter import XMLFormatter

from requests_html import HTML, HTMLSession


class UnsortedAttributes(XMLFormatter):
    def attributes(self, tag):
        for k, v in tag.attrs.items():
            yield k, v


def x(html, selector):
    out = html.find(selector)
    if len(out) > 0:
        text = out[0].text.strip()
        text = re.sub(r"[ \t]*\n[ \t]*", " ", text)
        return text
    else:
        return ""


g_clones = {
    "btown_2": "btowneyo",
    "pokedp_0": "pokedp",
}

g_overrides = {
    "293656c39f32d3ecb8a13ad033cb71443ecb52ad": "10masu",
    "ade6a02a516e87b21320def50f46d0414d35abac": "1nichi10",
    "6aabcdc5625d8510dc95e01eae32844900c2a8a5": "abcprk",
    "5c43debc3d00ad39e78d526c65f5c6c019c46e53": "advdrv",
    "1f575821a7d4ac2af96f1af031b10367ba6ade98": "anpaabc",
    "3e27953601f2d8ea69639f289c35c0917649e3b7": "anpasaga",
    "791bbb6a92fc7be0af4b9530890acd4a70458ed9": "anpatv",
    "53d85b60cea717dba39264c9530858fbc621237a": "anpawaku",
    "7488dddf64cb0c93b17235db93cf6a1bd5c05c0f": "asngnk",
    "316e0b863f393532b0ba67e0b4b8849603065255": "btown_2",
    "7fae650ccfb574a3d460a5a7e42cad099d06e7e4": "btowneyo",
    "772fd0455c7e39ef09395015094648c5f0fa62d5": "cars2",
    "928c4e497b0897240cc875a01aa5d1c101eb5757": "cinnamo",
    "20b3dfe395f7c97a32c93b580040787b98fd6024": "cookinbe",
    "d4a702e731de1f1870ba6c46e84e4475a6ce6247": "dorahika",
    "2269e145a004a9c6ccbb6e79c9aa25081a7ede2b": "doratsyh",
    "2139977a6a8cf870ef40622122d2d8c4e63047cd": "dorawwgl",
    "ea3dcaca3c1fcc31dbc58674238c7c32d12539ad": "dtanoshib",
    "19b216901a1c7b06bfc892b6df64f6efef569159": "dtanoshimt",
    "1b320737d49e6955f6a5e2f1876a18dd23b2856d": "engsengo",
    "b49914f97db95f611489487f558d13d7cceb3299": "frpc",
    "eea71480c063d9e7fced72c83dfb70b202bfeff5": "fwpcmh",
    "9990e7bbc8bd5b7bb5c7005dd38114c03f258048": "geneki",
    "ea99ab31fcab9c0bf99093cf63b3bded75ada95e": "ggsentai",
    "b12bd91e8ead04f558d32ce9086102a3c99f13eb": "hirakata",
    "0bce6e18b5b9ed3f74791a72cb8656b0f5dd18bc": "hkittyhk",
    "1e65579b74f141591db2122b34ce293d1d2120ff": "kazokum",
    "8ae4038dd35c04ba14d04e09ce71130957e0b22c": "kikatho",
    "566978b9e11dfc1d7a1012b56d8e638927f2eadb": "kouchuu",
    "134ebdfd67779400dfead05a6c94b28a0ba8df0e": "kounebu",
    "343c27c192da191d91fea43246da924b8f1af6ee": "mbubbub",
    "985844af2626ebcea56a6e764493d023d6cec293": "meiconan",
    "000adaa4d9c7404d32fc62ec82c3eafb710c7f64": "mezzopi",
    "82dd79cd81349d04614d16e8ad14413f797a5661": "miffy",
    "bb4242b187a7927408928b8bc2ee63be6c0f5285": "niasobo",
    "b0e137c8b03714280d8e049e1b8e2a3b850f3cbc": "odenkun",
    "2006c3fd2b662f4bb77388bfeefc2c885908fc5f": "osharem",
    "546b5c1e21b2c20120024153d201da8d9896e4c8": "oshrmoji",
    "ba2ed83d1caf05b0e84c39d2939b5ac1e11d160f": "partnertv",
    "26be7fd761042cb0af5bc10e5b34a3cf3f1f6d34": "pashah",
    "46e3c8f68b18f3b72cf0f3b404960e4534bd4eeb": "pointgt",
    "af27ec3b1480e1342c90ff7b15bcbac794241948": "pokebat",
    "866b56789fcb118e1ac50a8214a630a092aadd2f": "pokebw",
    "25aba3628c627c3ed603f14cb50cfe6101816db7": "pokedp",
    "bda5f00a03e43c3e03f1ea38bf96f6c276a54a0c": "pokedp_0",
    "24cda24c7b3053e004799793f4042d05e3859e1a": "pokemoji",
    "7bda85cab8e327d074bcf8ff55f9d001a6c15b31": "pc5gg",
    "29650349b117b96105882da7b12cac8fe9a04813": "precure5",
    "500ad94f4dda4b886ff1991ddc4a730d94b37cd6": "precurehc",
    "f0106c4a79f9aff9815318eadc272c7c62a50ec2": "sssbat",
    "9aef640c347430512e654cbf492d85eff1b94cb9": "suitepc",
    "ca44ac31ee66b5162eb43dd228664635b7d155c5": "toasobou",
    "0d89cdf8f9cf52ae8e1e981b8caf9a7c3cf7cb79": "toyst3",
    "02b615677a2ea81f77838ffdcf9d0856def52f3c": "tsentai",
}


def normalize_name(s):
    s = "".join(
        c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"
    )
    s = "".join(
        c for c in s if c in (string.ascii_letters + string.digits)
    )
    return s.lower()


def generate_software_entry(soup, html, romdirs):
    p = x(html, ".mw-parser-output > p")
    name_jp = re.search(r"\((.*?)\)", p).group(1)
    name_jp = re.sub(r" 1 ", "①", name_jp)
    name_jp = re.sub("([^A-Za-z0-9]) ([^A-Za-z0-9])", r"\1\2", name_jp)
    name_jp = re.sub(r" *and *", " and ", name_jp)
    name_jp = re.sub("&", "&amp;", name_jp)
    name_en = x(html, 'table.breakout span[itemprop="name"]')
    name_en = re.sub(r" o-", " O-", name_en)
    name_en = re.sub("&", "&amp;", name_en)
    publisher = x(html, 'table.breakout span[itemprop="publisher"]')
    if not publisher:
        publisher = x(html, 'table.breakout span[itemprop="author"]')
    datePublished = x(html, 'table.breakout span[itemprop="datePublished"]')
    datePublished = re.search(r"(.*?)-.*", datePublished).group(1)
    print(name_en)

    target_rom_dirs = []
    target_name = normalize_name(name_en)
    for d in romdirs:
        rom_file = next(d.glob("*.bin"))
        raw_names = [d.name, rom_file.name]
        for raw_name in raw_names:
            candidate_name = normalize_name(re.sub("&", "&amp;", raw_name))
            if target_name in candidate_name:
                target_rom_dirs.append(d)

    entries = []
    for target_rom_dir in target_rom_dirs:
        rom_file = next(target_rom_dir.glob("*.bin"))
        rom_data = open(rom_file, "rb").read()
        crc = zlib.crc32(rom_data) & 0xFFFFFFFF
        sha = hashlib.sha1(rom_data).hexdigest().lower()

        software_name = "12345678"
        if sha in g_overrides:
            software_name = g_overrides[sha]
        software_attrs = {"name": software_name, "supported": "no"}
        if software_name in g_clones:
            software_attrs["cloneof"] = g_clones[software_name]
        software = soup.new_tag("software", attrs=software_attrs)
        desc = soup.new_tag("description")
        desc.string = name_en
        year = soup.new_tag("year")
        year.string = datePublished
        publ = soup.new_tag("publisher")
        publ.string = publisher
        info = soup.new_tag("info", attrs={"name": "alt_title", "value": name_jp})
        software.append(desc)
        software.append(year)
        software.append(publ)
        software.append(info)

        part = soup.new_tag(
            "part", attrs={"name": "cart", "interface": "sega_beena_cart"}
        )
        dataarea = soup.new_tag(
            "dataarea",
            attrs={
                "name": "rom",
                "size": hex(len(rom_data)),
                "width": "32",
                "endianness": "big",
            },
        )
        rom_file_name = re.sub(" & ", " and ", re.sub("!", "", re.sub(r": ", "-", rom_file.name)))
        rom_file_name = "".join(
            c for c in unicodedata.normalize("NFD", rom_file_name) if unicodedata.category(c) != "Mn"
        )
        rom = soup.new_tag(
            "rom",
            attrs={
                "loadflag": "load16_word_swap",
                "name": rom_file_name,
                "size": hex(len(rom_data)),
                "crc": f"{crc:08x}",
                "sha1": sha,
            },
        )
        dataarea.append(rom)
        part.append(dataarea)

        booklet_dir = target_rom_dir
        if (target_rom_dir / "Booklet").exists():
            booklet_dir = target_rom_dir / "Booklet"
        for i, page_file in enumerate(sorted(booklet_dir.glob("*.png"))):
            rom_data = open(page_file, "rb").read()
            size = f"0x{len(rom_data):06x}"
            crc = zlib.crc32(rom_data) & 0xFFFFFFFF
            sha = hashlib.sha1(rom_data).hexdigest().lower()
            dataarea = soup.new_tag(
                "dataarea",
                attrs={
                    "name": f"page{i + 1}",
                    "size": size,
                },
            )
            rom = soup.new_tag(
                "rom",
                attrs={
                    "name": page_file.name,
                    "size": f"0x{len(rom_data):06x}",
                    "crc": f"{crc:08x}",
                    "sha1": sha,
                },
            )
            dataarea.append(rom)
            part.append(dataarea)

        software.append(part)

        entries.append({"name": software_name, "data": software})

    return entries


if __name__ == "__main__":
    rompath = sys.argv[1]
    romdirs = []
    for p in pathlib.Path(rompath).iterdir():
        if not p.is_file():
            romdirs.append(p)

    software_lineup_dir = pathlib.Path("software_lineup")
    software_lineup_dir.mkdir(parents=True, exist_ok=True)

    if len(sys.argv) > 2:
        with open(sys.argv[2], "r") as f:
            urls = f.readlines()
        for url in urls:
            print("GET", url)
            session = HTMLSession()
            response = session.get(url.strip())
            with open(
                str(software_lineup_dir.resolve()) + "/" + str(time.time_ns()) + ".html", "w"
            ) as f:
                # response.html.render()
                f.write(response.html.html)
            time.sleep(random.uniform(1000, 5000) / 1000)

    soup = BeautifulSoup(
        features="xml", preserve_whitespace_tags=["description", "year", "publisher"]
    )

    softwarelist = soup.new_tag(
        "softwarelist",
        attrs={"name": "sega_beena_cart", "description": "Sega Beena cartridges"},
    )
    software_entries = {}
    for h in sorted(software_lineup_dir.glob("*.html")):
        if h.is_file():
            with open(h, "r") as f:
                print(f"Processing {h.name}...")
                for entry in generate_software_entry(soup, HTML(html=f.read()), romdirs):
                    software_entries[entry["name"]] = entry["data"]
    for _, data in collections.OrderedDict(sorted(software_entries.items())).items():
        softwarelist.append(data)

    soup.append(softwarelist)

    out_filename = "out.xml"
    formatter = UnsortedAttributes(indent=4)
    with open(out_filename, "w") as f:
        f.write(soup.prettify(formatter=formatter))

    out = ""
    with open(out_filename, "r") as f:
        for line in f.readlines():
            if re.match(re.escape('<?xml version="1.0" encoding="utf-8"?>'), line):
                line = '<?xml version="1.0"?>\n'
                line += '<!DOCTYPE softwarelist SYSTEM "softwarelist.dtd">\n'
                line += "<!--\n"
                line += "license:CC0-1.0\n"
                line += "-->\n"
            elif re.match(re.escape("<softwarelist"), line):
                line = line + "	<!-- you must byteswap ROMs to see text, due to endian? -->\n\n"
            elif re.match("^[ \t]*" + re.escape("</software>"), line):
                line = line + "\n"
            for i in range(20, 1, -1):
                line = re.sub("^" + (4 * i * " "), i * "\t", line)
            line = re.sub("^    ", "\t", line)
            out += line

    with open(out_filename, "w") as f:
        f.write(out)
