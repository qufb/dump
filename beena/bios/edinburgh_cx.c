#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define BEENA_8M_MASK_SIZE 0x800000

uint8_t rom[BEENA_8M_MASK_SIZE];

uint32_t dcx(uint8_t *dst, uint8_t *src)
{
    uint32_t i;
    uint8_t *pbVar1;
    uint8_t *pbVar2;
    uint8_t *p_src;
    uint8_t *p_dst;
    uint8_t *pbVar3;
    uint8_t *pbVar4;
    uint32_t uVar5;
    int iVar6;
    int rep_count;
    uint32_t dcx_len;
    uint8_t bVar7;
    uint8_t bVar8;
    uint8_t b_src;

    b_src = *src;
    dcx_len = (uint32_t)src[1] * 0x10000 + (uint32_t)src[2] * 0x100 + (uint32_t)src[3];
    if ((uint32_t)b_src * 0x10000000 != ((uint32_t)src[1] + (uint32_t)src[2] + (uint32_t)src[3] + -1) * 0x10000000) {
        return 0;
    }
    if (dst == (uint8_t *)0x0) {
        return 0;
    }
    p_src = src + 4;
    i = b_src & 0x70;
    if ((b_src & 0x70) == 0) {
        // Uncompressed (direct copy of all bytes)
        i = 0;
        do {
            dst[i] = p_src[i];
            i = i + 1;
        } while (i < dcx_len);
    }
    else if (i == 0x10) {
        src = src + 5;
        uVar5 = (uint32_t) * p_src;
        i = 0x80;
        p_dst = dst;
        do {
            b_src = *src;
            if ((uVar5 & i) == 0) {
                // Direct copy
                pbVar1 = src + 1;
                pbVar3 = p_dst + 1;
                *p_dst = b_src;
                printf("1 %p=%02X\n", p_dst, b_src);
            }
            else {
                // Repeat from distance
                pbVar1 = src + 2;
                rep_count = (b_src & 0xf) + 2;
                pbVar2 = p_dst + (((uint64_t)-1) - ((uint64_t)src[1] + (b_src & 0xf0) * 0x10));
                do {
                    rep_count = rep_count + -1;
                    pbVar3 = p_dst + 1;
                    *p_dst = *pbVar2;
                    printf("2 %p=[%p]=%02X\n", p_dst, pbVar2, *pbVar2);
                    p_dst = pbVar3;
                    pbVar2 = pbVar2 + 1;
                } while (-1 < rep_count);
            }
            i = i >> 1;
            src = pbVar1;
            if (i == 0) {
                i = 0x80;
                src = pbVar1 + 1;
                uVar5 = (uint32_t) * pbVar1;
            }
            p_dst = pbVar3;
        } while (pbVar3 < dst + dcx_len);
    }
    else if (i == 0x20) {
        src = src + 5;
        uVar5 = (uint32_t) * p_src;
        p_src = dst + dcx_len;
        i = 0x80;
        do {
            if ((uVar5 & i) == 0) {
                *dst = *src;
                pbVar3 = src + 2;
                dst[1] = src[1];
            }
            else {
                b_src = *src;
                pbVar3 = src + 1;
                if (b_src < 0x80) {
                    bVar7 = 0;
                }
                else {
                    bVar7 = 0xff;
                }
                *dst = bVar7;
                dst[1] = b_src;
            }
            i = i >> 1;
            src = pbVar3;
            if (i == 0) {
                i = 0x80;
                src = pbVar3 + 1;
                uVar5 = (uint32_t) * pbVar3;
            }
            dst = dst + 2;
        } while (dst < p_src);
    }
    else {
        if (i != 0x30) {
            return 0;
        }
        iVar6 = 0;
        do {
            pbVar1 = dst + iVar6;
            pbVar3 = p_src;
            do {
                pbVar4 = pbVar3 + 1;
                b_src = *pbVar3;
                i = (uint32_t)(b_src >> 2);
                bVar7 = b_src & 3;
                pbVar2 = pbVar1;
                if ((b_src & 3) == 0) {
                    do {
                        bVar8 = i != 0;
                        i = i - 1;
                        pbVar1 = pbVar2 + 2;
                        *pbVar2 = *pbVar4;
                        pbVar2 = pbVar1;
                        p_src = pbVar4 + 1;
                        pbVar4 = pbVar4 + 1;
                    } while (bVar8);
                }
                else {
                    p_src = pbVar4;
                    if (bVar7 == 1) {
                        do {
                            pbVar1 = pbVar2 + 2;
                            *pbVar2 = 0;
                            bVar8 = i != 0;
                            i = i - 1;
                            pbVar2 = pbVar1;
                        } while (bVar8);
                    }
                    else if (bVar7 == 2) {
                        p_src = pbVar3 + 2;
                        b_src = *pbVar4;
                        pbVar3 = pbVar1;
                        do {
                            pbVar1 = pbVar3 + 2;
                            *pbVar3 = b_src;
                            bVar8 = i != 0;
                            i = i - 1;
                            pbVar3 = pbVar1;
                        } while (bVar8);
                    }
                    else if (bVar7 == 3) {
                        b_src = *pbVar4;
                        do {
                            pbVar1 = pbVar2 + 2;
                            *pbVar2 = b_src;
                            bVar8 = i != 0;
                            i = i - 1;
                            pbVar2 = pbVar1;
                            p_src = pbVar3 + 2;
                            b_src = b_src + 1;
                        } while (bVar8);
                    }
                }
                pbVar3 = p_src;
            } while (pbVar1 < dst + dcx_len);
            iVar6 = iVar6 + 1;
        } while (iVar6 < 2);
    }
    return dcx_len;
}

void dcx_at_offset(char *gfx_offset, char *out_name) {
    printf("processing %s...\n", out_name);

    uint8_t dst[0x10000];
    uint8_t *src = &rom[0];
    src += (int) strtol(gfx_offset, NULL, 0);
    uint8_t dcx_meta = *src;
    uint8_t dcx_type = dcx_meta & 0x70;
    printf("- cpx meta=0x%02X, type=0x%02X\n", dcx_meta, dcx_type);

    uint32_t expected_len = (uint32_t)src[1] * 0x10000 + (uint32_t)src[2] * 0x100 + (uint32_t)src[3];
    uint32_t got_len = dcx(dst, src);
    printf("- size expected=0x%lX, got=0x%lX\n", expected_len, got_len);
    printf("- dcx head=0x%08X\n", *(uint32_t*)dst);

    FILE *f = f = fopen(out_name, "wb");
    fwrite(dst, got_len, 1, f);
    fclose(f);
}

int main(int argc, char *argv[]) {
    if (argc < 3) {
        fprintf(stderr, "Usage: %s rom.bin tiles_hex_rom_offset [meta_hex_rom_offset]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    FILE *f = fopen(argv[1], "rb");
    fseek(f, 0, SEEK_END);
    uint32_t fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    fread(rom, fsize, 1, f);
    fclose(f);

    //                  warn: meta @ 0x8004d2fc; tiles @ 0x8004ce02;
    //                bandai: meta @ 0x8073eb52; tiles @ 0x8073e42e;
    //             copyright: meta @ 0x807401c6; tiles @ 0x8073ec22;
    // testversion_titlelogo: meta @ 0x80745620; tiles @ 0x80740246;
    dcx_at_offset(argv[2], "out2.gfx_tiles.bin");
    if (argc > 3) {
        dcx_at_offset(argv[3], "out2.gfx_meta.bin");
    }

    return 0;
}
