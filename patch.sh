#!/bin/sh

set -eu

# Read original name of binary file provided as argument
if [ $# -lt 1 ]; then
  echo "Usage: ./patch.sh FILE" >&2
  exit 1
fi
bin=$1

# Make hex dump of binary files and diff if it doesn't exist
if [ ! -f "$bin".hex.diff ]; then
  xxd -g 1 "$bin".0 > "$bin".0.hex
  xxd -g 1 "$bin" > "$bin".hex

  sha1sum "$bin".0 > "$bin".0.sha1
  sha1sum "$bin" > "$bin".sha1

  # Create diff file, gracefully handling exit status: 0 if inputs are the same, 1 if different, 2 if trouble.
  set +e
  diff -u0 "$bin".0.hex "$bin".hex > "$bin".hex.diff
  status=$?
  set -e
  if [ $status -gt 1 ]; then
    echo "Could not create diff." >&2
    exit $status
  fi

  echo "Created diff. Run again with original '$bin' to apply diff."
  exit 0
fi

echo "Press ENTER to overwrite '$bin' (backup stored in '$bin.0')." >&2
read -r _

cp "$bin" "$bin".0

sha1sum --check "$bin".0.sha1

# Apply diff file and re-create binary file
xxd -g 1 "$bin" > "$bin".hex
patch -i "$bin".hex.diff
xxd -r -g 1 "$bin".hex > "$bin"

sha1sum --check "$bin".sha1
