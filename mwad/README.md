# Export Models

1. Clone FrogLord's [MediEvil branch](https://github.com/Kneesnap/FrogLord/tree/medievil)
2. Configure $PATH for a JDK with class `javafx/application/Application` (e.g. Oracle JDK8)
3. Run: `mvn package && java -jar target/editor-0.5.0-jar-with-dependencies.jar`
4. Navigate to WAD Files > Select WAD File > Select menu "File > Export Alternate Format"
5. Import `.obj` files in Blender: [items.blend](./items.blend)
