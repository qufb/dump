-- license:BSD-3-Clause
-- copyright-holders:MAMEdev Team

---------------------------------------------------------------------------
--
--   mattel.lua
--
--   Small driver-specific example makefile
--   Use make SUBTARGET=mattel to build
--
---------------------------------------------------------------------------


--------------------------------------------------
-- Specify all the CPU cores necessary for the
-- drivers referenced in mattel.lst.
--------------------------------------------------

CPUS["ARM7"] = true

--------------------------------------------------
-- Specify all the sound cores necessary for the
-- drivers referenced in mattel.lst.
--------------------------------------------------

-- SOUNDS["YM2154"] = true


--------------------------------------------------
-- specify available video cores
--------------------------------------------------

-- VIDEOS["SEGA315_5313"] = true


--------------------------------------------------
-- specify available machine cores
--------------------------------------------------

-- MACHINES["INPUT_MERGER"] = true


--------------------------------------------------
-- specify available bus cores
--------------------------------------------------

-- BUSES["MEGADRIVE"] = true


--------------------------------------------------
-- This is the list of files that are necessary
-- for building all of the drivers referenced
-- in mattel.lst
--------------------------------------------------

function createProjects_mame_mattel(_target, _subtarget)
	project ("mame_pixtermu")
	targetsubdir(_target .."_" .. _subtarget)
	kind (LIBTYPE)
	uuid (os.uuid("drv-mame-mattel"))
	addprojectflags()
	precompiledheaders_novs()

	includedirs {
		MAME_DIR .. "src/osd",
		MAME_DIR .. "src/emu",
		MAME_DIR .. "src/devices",
		MAME_DIR .. "src/mame/shared",
		MAME_DIR .. "src/lib",
		MAME_DIR .. "src/lib/util",
		MAME_DIR .. "3rdparty",
		GEN_DIR  .. "mame/layout",
	}

files{
	MAME_DIR .. "src/mame/mattel/pixtermu.cpp",
}
end

function linkProjects_mame_mattel(_target, _subtarget)
	links {
		"mame_pixtermu",
	}
end
