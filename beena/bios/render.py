#!/usr/bin/env python3

from PIL import Image
import argparse
import math
import numpy as np
import random
import struct
import sys

from edinburgh_cx import dcx_at_offset

"""
Examples:

# Obtaining memory dumps via MAME debugger
dump foo_0x40020000.dmp,0x40020000,0x1000
dump foo_0xc0000000.dmp,0xc0000000,0xc6000
dump foo_0xc00c8000.dmp,0xc00c8000,0x2000
dump foo_0xc00ca000.dmp,0xc00ca000,0x2000
dump foo_0xc0100000.dmp,0xc0100000,0x80000
dump foo_0xc0180000.dmp,0xc0180000,0x80000

# Direct bitmap
./render.py \
    --tiles cooking_0xc0000000.dmp \
    --direct \
    --scale=2

# Sprites (no tile data passed)
./render.py \
    --tiles tv.0x29734.tilemap.bin \
    --palette tv_0x40020000.dmp

# Full bitplane
./render.py \
    --tiles pokebw_title_0xc0100000.dmp \
    --tilemap pokebw_title_0xc00c8000.dmp \
    --palette pokebw_title_0x40020000.dmp

# No cart found
./render.py \
    --rom beenabios.bin \
    --tiles 0xc0100000.dmp \
    --tilemap 0xc00c8000.dmp \
    --palette_offset 0xdd8c \
    --bg=0xc5c5c5 \
    --fit

# Pre-decompressed
./render.py \
    --rom S-100002-1200.bin \
    --tiles sorei.testversion_titlelogo.meta.bin \
    --tilemap sorei.testversion_titlelogo.tilemap.bin \
    --palette_offset 0x745730 \
    --bg=0 \
    --raw

# Compressed
./render.py \
    --rom S-100002-1200.bin \
    --tiles_offset 0x740246 \
    --tilemap_offset 0x745620 \
    --palette_offset 0x745730 \
    --bg=0 \
    --raw \
    --out=sorei.testversion_titlelogo.dcx.png

# Compose
./render.py \
   --rom S-100042-1000.bin \
   --tiles_offset 0x52a23 \
   --tilemap_offset 0x55c9c \
   --palette_offset 0x4c67c 0x4c71c \
   --bg=0 \
   --raw \
   --out=pokebw.ribbons.dcx.png
"""


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


def swap_16(data):
    """Swap bytes for each word-sized chunk."""
    l = len(data) & ~1
    data[1:l:2], data[:l:2] = data[:l:2], data[1:l:2]
    return data


def to_rgb(num_colors, colors):
    """Convert from BGR555 to RGB888."""
    rgb_colors = []
    for i in range(0, num_colors, 2):
        color = colors[i : i + 2]
        color = int.from_bytes(color, byteorder="big")
        b = (color >> 10) & 0b11111
        g = (color >> 5) & 0b11111
        r = color & 0b11111
        rgb = [int(x * (255 / 31)) for x in [r, g, b]]
        rgb_colors.append(rgb)
    return rgb_colors


def to_tiles(args, rgb_colors, gfx_tilemap, gfx_tiles, tile_len):
    if args.raw:
        # First tile is always blank, but doesn't get stored with
        # the rest of tile data, we need to manually insert it
        gfx_tiles = (
            b"".join(b"\x00" for _ in range(tile_len * tile_len)) + gfx_tiles
        )

    rgb_pixels = []
    tilemap_count = args.width * args.height
    tile_size = tile_len * tile_len
    tile_data = list(chunks(gfx_tilemap, 2))
    for tile_data_i, tile_data_entry in enumerate(tile_data):
        # Only show tiles in visible area
        if args.fit and (
            (tile_data_i < 64 * 2)
            or ((tile_data_i % 64) < 10)
            or ((tile_data_i % 64) > 44 + 10 - 1)
            or (tile_data_i > 64 * 32)
        ):
            continue

        i = int.from_bytes(tile_data_entry, byteorder="big") & 0xFFF
        tile_offset = tile_size * i

        # Reorder indexes if tile should be flipped
        x_range = range(0, tile_len, 1)
        y_range = range(0, tile_len, 1)
        transform = (int.from_bytes(tile_data_entry, byteorder="big") >> 12) & 0xF
        if transform & 1 != 0:
            x_range = range(tile_len - 1, -1, -1)
        if transform & 2 != 0:
            y_range = range(tile_len - 1, -1, -1)

        for y in y_range:
            for x in x_range:
                pal_i = gfx_tiles[tile_offset + (y * tile_len) + x]
                rgb_pixels.append(rgb_colors[pal_i])

    return list(chunks(rgb_pixels, tile_len * tile_len))


def render(args):
    if args.rom:
        rom_bytes = args.rom.read()
        if rom_bytes[0x20:0x28] == b"deniubgr":
            rom_bytes = swap_16(bytearray(rom_bytes))
    else:
        rom_bytes = None

    if args.tiles_offset and len(args.tiles_offset) > 0:
        gfx_tiles = b""
        for offset in args.tiles_offset:
            gfx_tiles += dcx_at_offset(rom_bytes, offset)
    else:
        gfx_tiles = args.tiles.read()

    if args.tilemap_offset and len(args.tilemap_offset) > 0:
        gfx_tilemap = b""
        for offset in args.tilemap_offset:
            gfx_tilemap += dcx_at_offset(rom_bytes, offset)
    elif args.tilemap:
        gfx_tilemap = args.tilemap.read()
    else:
        # If no tilemap present, generate enough indexes to cover
        # all available tiles, arranged in a square layout
        tiles_count = len(gfx_tiles) // 0x100
        size_factor = math.ceil(math.sqrt(tiles_count))
        w = size_factor
        h = size_factor
        gfx_tilemap = struct.pack(">HH", w, h) + b"".join(
            struct.pack(">H", i) for i in range(tiles_count)
        )
        args.raw = True

    if args.direct:
        w = 352 * args.scale
        h = 240 * args.scale
        tile_len = 1
        rgb_colors = to_rgb(len(gfx_tiles), gfx_tiles)
        rgb_tiles = list(chunks(rgb_colors, tile_len * tile_len))
        idata = np.full(
            (((len(rgb_tiles) // w) + 1) * tile_len, w * tile_len, 3),
            0xFF,
            dtype=np.uint8,
        )
    else:
        # Defaults to bitplane size
        w = 64
        h = 64
        if args.raw:
            w = int.from_bytes(gfx_tilemap[0:2], byteorder="big")
            h = int.from_bytes(gfx_tilemap[2:4], byteorder="big")
        elif args.fit:
            w = 44
            h = 30
        if args.width:
            w = args.width
        else:
            args.width = w
        if args.height:
            h = args.height
        else:
            args.height = h
        print(f"Dimensions: w={w}, h={h}")

        tile_len = 16
        if args.palette:
            colors = args.palette.read()
        elif args.palette_offset and len(args.palette_offset) > 0:
            colors = b""
            for offset in args.palette_offset:
                num_colors = int.from_bytes(
                    rom_bytes[offset : offset + 2], byteorder="big"
                )
                colors += rom_bytes[offset + 2 : offset + 2 + num_colors * 2]
        else:
            # If no palette data present, generate random max number of entries
            colors = b"".join(
                struct.pack(">H", random.randint(0, 0xFFFF)) for _ in range(0x100)
            )
        rgb_colors = to_rgb(len(colors), colors)
        if args.bg is not None:
            # Add background color to raw palette. Games have it set as
            # a global variable, so it's usually missing from these entries.
            # Effectively used as transparency.
            bg = [
                (args.bg >> 16) & 0xFF,
                (args.bg >> 8) & 0xFF,
                (args.bg) & 0xFF,
            ]
            rgb_colors.insert(0, bg)
        rgb_tiles = to_tiles(
            args,
            rgb_colors,
            gfx_tilemap[4:] if args.raw else gfx_tilemap,
            gfx_tiles,
            tile_len,
        )
        idata = np.full((h * tile_len, w * tile_len, 3), 0xFF, dtype=np.uint8)

    if len(rgb_tiles) == 0:
        raise RuntimeError("No tiles were parsed!")

    for ti in range(len(rgb_tiles)):
        wi = ti % w
        hi = ti // w
        for i in range(0, tile_len):
            for j in range(0, tile_len):
                v = rgb_tiles[ti][i + (j * tile_len)]
                idata[(hi * tile_len) + j, (wi * tile_len) + i] = tuple(v)

    img = Image.fromarray(idata, "RGB")
    img.show()
    if args.out:
        img.save(args.out)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--rom",
        type=argparse.FileType("rb"),
        help="game or BIOS ROM",
    )
    parser.add_argument(
        "--tilemap",
        type=argparse.FileType("rb"),
        help="memory dump of decompressed tilemap data",
    )
    parser.add_argument(
        "--tiles",
        type=argparse.FileType("rb"),
        help="memory dump of decompressed tiles entries",
    )
    parser.add_argument(
        "--tilemap_offset",
        nargs="+",
        type=lambda x: int(x, 0),
        help="ROM file offset where compressed tilemap is stored (e.g. 0x123)",
    )
    parser.add_argument(
        "--tiles_offset",
        type=lambda x: int(x, 0),
        nargs="+",
        help="ROM file offset where compressed tiles are stored (e.g. 0x123)",
    )
    parser.add_argument(
        "--palette",
        type=argparse.FileType("rb"),
        help="memory dump of palette entries",
    )
    #                  warn @ 0x4cdf8
    #                bandai @ 0x73ec18
    #             copyright @ 0x740222
    # testversion_titlelogo @ 0x745730
    parser.add_argument(
        "--palette_offset",
        type=lambda x: int(x, 0),
        nargs="+",
        help="ROM file offset where palette is stored (e.g. 0x123), use if palette memory dump is not passed",
    )
    parser.add_argument(
        "--bg",
        type=lambda x: int(x, 0),
        help="background color in hex (e.g. 0x123456)",
    )
    parser.add_argument(
        "--width",
        type=int,
        help="width in number of tiles",
    )
    parser.add_argument(
        "--height",
        type=int,
        help="height in number of tiles",
    )
    parser.add_argument(
        "--scale",
        type=int,
        default=1,
        help="multiplier for direct bitmap size (1 = 352*240, 2 = 704*480)",
    )
    parser.add_argument(
        "--fit",
        default=False,
        action="store_true",
        help="true if rendered tiles should be limited to visible area",
    )
    parser.add_argument(
        "--direct",
        default=False,
        action="store_true",
        help="true if rendering a direct bitmap (i.e. palette entries are hardcoded in tile data)",
    )
    parser.add_argument(
        "--raw",
        default=False,
        action="store_true",
        help="true if rendering tile data extracted from ROM (i.e. not from memory dumps)",
    )
    parser.add_argument(
        "--out",
        type=str,
        help="output image filename",
    )
    render(parser.parse_args())
