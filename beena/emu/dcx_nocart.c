#include <unicorn/unicorn.h>
#include <udbserver.h>
#include <stdio.h>
#include <stdlib.h>

#define BEENA_4M_MASK_SIZE 0x400000
#define BEENA_8M_MASK_SIZE 0x800000
#define BIOS_SIZE 0x20000

unsigned char bios[BIOS_SIZE];

static bool hook_mem_fetch_unmapped(uc_engine *uc, uint64_t address) {
    return false;
}

static bool hook_mem_invalid(uc_engine *uc, uc_mem_type type, uint64_t address, int size, int64_t value, void *user_data) {
    switch (type) {
    case UC_MEM_READ_UNMAPPED:
        printf("! UC_MEM_READ_UNMAPPED @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_WRITE_UNMAPPED:
        printf("! UC_MEM_WRITE_UNMAPPED @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_FETCH_UNMAPPED:
        printf("! UC_MEM_FETCH_UNMAPPED @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_READ_PROT:
        printf("! UC_MEM_READ_PROT @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_WRITE_PROT:
        printf("! UC_MEM_WRITE_PROT @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    case UC_MEM_FETCH_PROT:
        printf("! UC_MEM_FETCH_PROT @ 0x%" PRIx64 ", size = %u, value = 0x%" PRIx64 "\n", address, size, value);
        return false;
    default:
        printf("! UC_MEM @ 0x%" PRIx64 "\n", address);
        return false;
    }
}

static void hook_code(uc_engine *uc, uint64_t address, uint32_t size, void *user_data) {
    printf("> trace @ 0x%" PRIx64 "\n", address, size);
    hook_mem_fetch_unmapped(uc, address);
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s bios.bin\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    FILE *f = fopen(argv[1], "rb");
    fseek(f, 0, SEEK_END);
    unsigned long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);
    fread(bios, fsize, 1, f);
    fclose(f);

    // bios' dcx()
    unsigned long start = 0x62ac;
    unsigned long until = 0x64bc;

    uc_engine *uc;
    uc_hook trace1, trace2, trace3;
    uc_open(UC_ARCH_ARM, UC_MODE_BIG_ENDIAN, &uc);

    uc_mem_map(uc, 0x00000000L, 0x20000L, UC_PROT_READ | UC_PROT_EXEC);
    uc_mem_map(uc, 0x20000000L, 0xF0000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x40000000L, 0xF0000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x50000000L, 0xF0000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x60000000L, 0xF0000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x70000000L, 0xF0000L, UC_PROT_ALL);
    uc_mem_map(uc, 0x80000000L, BEENA_8M_MASK_SIZE, UC_PROT_READ | UC_PROT_EXEC);
    uc_mem_map(uc, 0xA0FFC000L, 0x4000L, UC_PROT_ALL);
    uc_mem_map(uc, 0xC0000000L, 0xC0000L, UC_PROT_ALL);
    uc_mem_map(uc, 0xC00C0000L, 0x1F0000L, UC_PROT_ALL);

    uc_mem_write(uc, 0x00000000L, bios, BIOS_SIZE);

    // Patch return instruction, just in case...
    const unsigned char loop[4] =  {0xea, 0xff, 0xff, 0xfe};
    uc_mem_write(uc, 0x64c0, loop, 0x4);

    unsigned long sp = 0xC00FFF80L;
    uc_reg_write(uc, UC_ARM_REG_SP, &sp);

    uc_hook_add(uc, &trace1, UC_HOOK_CODE, hook_code, NULL, 1, 0);
    //uc_hook_add(uc, &trace2, UC_HOOK_MEM_READ, hook_mem_read, NULL, 1, 0);
    uc_hook_add(uc, &trace3,
        UC_HOOK_MEM_READ_PROT |
        UC_HOOK_MEM_WRITE_PROT |
        UC_HOOK_MEM_FETCH_PROT |
        UC_HOOK_MEM_READ_UNMAPPED |
        UC_HOOK_MEM_WRITE_UNMAPPED |
        UC_HOOK_MEM_FETCH_UNMAPPED |
        UC_HOOK_MEM_UNMAPPED,
        hook_mem_invalid, NULL, 1, 0);

    //udbserver(uc, 12345, start);

    unsigned long nocart_pal = 0xdd8c;
    unsigned long nocart_meta = 0xdda8;
    unsigned long nocart_meta_size = 0x34;
    unsigned long nocart_tiles = 0xddd4;
    unsigned long nocart_tiles_size = 0x1000;
    unsigned long w_nocart_meta = 0xc0100100;
    unsigned long w_nocart_tiles = 0xc0000100;

    uc_reg_write(uc, UC_ARM_REG_R0, &w_nocart_meta);
    uc_reg_write(uc, UC_ARM_REG_R1, &nocart_meta);
    uc_emu_start(uc, start, until, 0, 0);

    unsigned char nocart_meta_dcx[nocart_meta_size];
    if (uc_mem_read(uc, w_nocart_meta, &nocart_meta_dcx, nocart_meta_size) != UC_ERR_OK) {
        printf("uc_mem_read fail for address: 0x%" PRIx64 "\n", w_nocart_meta);
    }
    f = fopen("out.nocart_meta.bin", "wb");
    fwrite(nocart_meta_dcx, nocart_meta_size, 1, f);
    fclose(f);

    uc_reg_write(uc, UC_ARM_REG_R0, &w_nocart_tiles);
    uc_reg_write(uc, UC_ARM_REG_R1, &nocart_tiles);
    uc_emu_start(uc, start, until, 0, 0);

    unsigned char nocart_tiles_dcx[nocart_tiles_size];
    if (uc_mem_read(uc, w_nocart_tiles, &nocart_tiles_dcx, nocart_tiles_size) != UC_ERR_OK) {
        printf("uc_mem_read fail for address: 0x%" PRIx64 "\n", w_nocart_tiles);
    }
    f = fopen("out.nocart_tiles.bin", "wb");
    fwrite(nocart_tiles_dcx, nocart_tiles_size, 1, f);
    fclose(f);

    return 0;
}
